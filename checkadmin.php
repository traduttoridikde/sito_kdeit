<?php
/**
 * Modulo di identificazione specifico per amministratori
 */

// Controllo che impedisce il richiamo diretto del file include dall'esterno.
if( !defined('ASSOLI_SCRIPT') )
 {
     header('HTTP/1.1 404 Not Found');
     echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n<html><head>".
          "\n<title>404 Not Found</title>\n</head>";
     echo "<body>\n<h1>Not Found</h1>\n<p>The requested URL ".
          $_SERVER['REQUEST_URI']." was not found on this server.</p>\n";
     echo "<hr>\n".$_SERVER['SERVER_SIGNATURE']."\n</body></html>\n";
     exit;
}

require_once( 'configurazione.php' );

if ( !isset($_SERVER['PHP_AUTH_USER']) ) {
    Header("WWW-Authenticate: Basic realm=\"KDE i18n - Italian team\"");
    Header("HTTP/1.0 401 Unauthorized");
    include_once( 'header.php' );
    echo '<h1>Accesso non autorizzato</h1>';
    echo '<br/>';
    echo '<p class="it_mainpage">Devi <a href="register.php">registrarti</a> '.
         'per accedere a queste funzioni.</p>';
    include_once( 'footer.php' );
    exit;
} else {
    require_once( 'connessione.php' );

    $query = "SELECT password, admin FROM translators WHERE email= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('s', $_SERVER['PHP_AUTH_USER']);
    $stmt->execute();
    $stmt->bind_result($pass, $isAdmin);
    $stmt->fetch();
    $stmt->close();

    if ( !(crypt($_SERVER['PHP_AUTH_PW'], $pass)== $pass) ) {
        Header("WWW-Authenticate: Basic realm=\"KDE i18n - Italian team\"");
        Header("HTTP/1.0 401 Unauthorized");

        // Disattiva variabile altrimenti si vedrà il menù dell'utente
        unset( $_SERVER['PHP_AUTH_USER'] );
        include_once( 'header.php' );
        echo "<h1>Accesso non autorizzato</h1>" ;
        echo "<br />" ;
        echo "<p class=\"it_mainpage\"><strong>Errore</strong>: ".
             "Nome utente o password non validi.</p>";
        include_once( 'footer.php' );
        exit;
    } elseif( !$isAdmin ) {
        Header("WWW-Authenticate: Basic realm=\"KDE i18n - Italian team\"");
        Header("HTTP/1.0 401 Unauthorized");

        include_once( 'header.php' );
        echo "<h1>Accesso non autorizzato</h1>" ;
        echo "<br />" ;
        echo "<p class=\"it_mainpage\"><strong>Errore</strong>: ".
             "Non sei un amministratore.</p>";
        include_once( 'footer.php' );
        exit;
    }
}
?>