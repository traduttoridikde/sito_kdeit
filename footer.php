<?php
/**
*
* File con il contenuto del footer delle pagine
*
*/

// Controllo che impedisce il richiamo diretto del file include dall'esterno.
if( !defined('ASSOLI_SCRIPT') )
 {
     header('HTTP/1.1 404 Not Found');
     echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n<html><head>\n<title>404 Not Found</title>\n</head>";
     echo "<body>\n<h1>Not Found</h1>\n<p>The requested URL ".$_SERVER['REQUEST_URI']." was not found on this server.</p>\n";
     echo "<hr>\n".$_SERVER['SERVER_SIGNATURE']."\n</body></html>\n";
     exit;
}

?>
<!-- Inizio footer -->

</div>

<div class="clearer"></div>

</div>
<div class="clearer"></div>
</div>

<div id="end_body"></div>

<div id="footer">
<p><a href="https://validator.w3.org/check?uri=referer">XHTML 1.1</a> e
<a href="https://jigsaw.w3.org/css-validator/check?uri=referer">CSS</a>
validi.<br/>
<?php
    exec( "env LC_ALL=C git log -1 --format='%cd'", $ultimaModifica );
    $data = strftime("%e %B %Y, %H:%m (%Z)", strtotime( $ultimaModifica[0] ) );
    echo "<span style=\"font-size: smaller;\">Ultima modifica: $data</span>\n";
?>
<br/>
<a href="affero.php">Codice del sito disponibile sotto licenza Affero
GPL 3.0</a>
</p>
</div>

</body>
</html>
