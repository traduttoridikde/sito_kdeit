<?php
/**
 * File di visualizzazione traduttori con informazioni di contatto, breve
 * biografia e statistiche generali.
 */

define('ASSOLI_SCRIPT', 1);

include("functions.php");
include("header.php");
require_once( 'configurazione.php' );
require_once( 'connessione.php' );

// NOTE: Questa query salta tutti i traduttori che non abbiano file assegnati.
//       È BENE COSÌ! Altrimenti diamo spazio a spammer e gente che si vuol fare
//       pubblicità come traduttore.
$query = "SELECT idTranslator, CONCAT_WS(' ', first, last) AS name, email, ".
         "biography, tipofoto, SUM(done) AS fatti, SUM(fuzzy) AS fuzzy, ".
         "SUM(todo) AS dafare, COUNT(*) AS numerofile ".
         "FROM translators NATURAL JOIN files NATURAL JOIN stats ".
         "GROUP BY idTranslator ORDER BY first, last";
$results = $db->query($query);

echo "<h1>I traduttori italiani di KDE</h1>\n";

while ($result = $results->fetch_assoc()) {

    $id = $result["idTranslator"];
    $name = $result["name"];
    $email = antispam($result["email"]);
    $bio = $result["biography"];
    $tipofoto = $result["tipofoto"];
    $fatti = $result["fatti"];
    $fuzzy = $result["fuzzy"];
    $dafare = $result["dafare"];
    $numerofile = $result["numerofile"];

    echo "<div class=\"it_trad\">\n";

    echo "<h2 id=\"id-$id\" style=\"color: white;\">\n" ;
    echo "<img src=\"foto.php";
    if( !empty($tipofoto) )
        echo "?id=$id";
    echo "\" alt=\"$name\" title=\"$name\" class=\"framed\"/>\n";
    echo "$name</h2>\n";
    echo "<p>Posta elettronica:</p><address>$email</address>\n";

    if($bio)
        echo "<p>Mini-biografia:<br />$bio</p>";

    echo "<div style=\"clear: right;\"></div>";
    echo "<fieldset class=\"it_feedback\" style=\"width: 30% ; ".
         "margin-left: 10px ; text-align: center;\">";
    echo "<legend>Statistiche del traduttore</legend>\n";
    echo "<table style=\"margin-left: auto; margin-right: auto;\">\n";
    echo "<tr>\n";
    echo "<td><div style=\"margin-left: auto; margin-right: auto;\">";
    statBar( 200, $result["fatti"], $result["fuzzy"], $result["dafare"] );
    echo "</div></td>\n";
    echo "</tr>\n";
    echo "<tr>\n";

    echo "<td>File: <strong>".number_format($numerofile,0,',','˙').
         "</strong></td>\n";
    echo "</tr>\n";
    echo "<tr>\n";
    $totali = $result["fatti"] + $result["fuzzy"] + $result["dafare"];
    echo "<td>Messaggi: <strong>".number_format($totali,0,',','˙').
         "</strong></td>\n";

    echo "</tr>\n";
    echo "</table>\n";
    echo "</fieldset>\n";
    echo "<table class=\"it_pack\">\n<tr>\n";
    echo "<th><a href=\"file.php?translator=$id&amp;status=unfinished\">".
         "Lavoro da fare</a></th>\n";
    echo "<th><a href=\"file.php?translator=$id\">Lista file</a></th>\n";
    echo "<th><a href=\"file.php?reviewer=$id\">Revisioni".
         "</a></th>\n";
    echo "</tr></table>";

    // Assicurati che il riquadro racchiuda tutta la foto
    echo "<div style=\"clear: right;\"></div>";
    echo "</div>\n\n";
}

include("footer.php");

?>