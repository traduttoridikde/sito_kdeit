<?php
/**
 * Funzioni di registrazione e modifica degli utenti.
 */

require_once( 'functions.php' );

// Controllo che impedisce il richiamo diretto del file include dall'esterno.
if( !defined('ASSOLI_SCRIPT') )
{
     header('HTTP/1.1 404 Not Found');
     echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n";
     echo "<html><head>\n<title>404 Not Found</title>\n</head>";
     echo "<body>\n<h1>Not Found</h1>\n<p>The requested URL ";
     echo $_SERVER['REQUEST_URI']." was not found on this server.</p>\n";
     echo "<hr>\n".$_SERVER['SERVER_SIGNATURE']."\n</body></html>\n";
     exit();
}

// Dato un id e una password, restituisce TRUE se valida o FALSE altrimenti
function checkUserPassword( $id, $password, $db ) {
    $query = "SELECT password FROM translators WHERE idTranslator= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $stmt->bind_result($stored);
    $stmt->fetch();
    return ( crypt($password, $stored) == $stored );
}

// Modifica i dati di un traduttore; se $id non viene passato, ne crea uno nuovo
// Nessuna verifica di sicurezza dei dati qui, vanno verificati all'entrata
function editUser( $first, $last, $email, $password, $file, $bio, $db,
                   $id = NULL ) {

    $hashed = crypt($password);

    $query  = $id ? "UPDATE " : "INSERT INTO ";
    $query .= "translators SET first= ?, last= ?, email= ?, ".
              "password=?, biography=?";
    $query .= $id ? " WHERE idTranslator= ?" : "";
    $stmt   = $db->prepare($query);
    if( $id )
        $stmt->bind_param('sssssi', $first, $last, $email, $hashed, $bio, $id);
    else
        $stmt->bind_param('sssss', $first, $last, $email, $hashed, $bio);
    $stmt->execute();
    $stmt->close();

    if( !$id ) {
        $query = "SELECT idTranslator FROM translators WHERE email= ^";
        $stmt  = $db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->bind_result($id);
        $stmt->fetch();
        $stmt->close();
    }

    if( $file ) {
        $codice_foto = setImage($file, $email, $db);
        if( $codice_foto )
            echo "<p><strong>$codice_foto</strong></p>";
    }
}

// Inserisce o modifica la foto di un utente
// Documentazione a: http://www.php-mysql-tutorial.com/php-mysql-upload.php
function setImage ($file, $email, $db) {

    if( $file["error"]==UPLOAD_ERR_NO_FILE )
        return "";

    if( $file["error"] )
        return "<p><a href=\"http://de2.php.net/manual/en/features.".
               "file-upload.errors.php\">Codice di errore ".
               $file["error"]."</a> nel caricamento dell'immagine.</p>";

    // Quanto segue di solito succede se non si dà un file.
    $filename = $file["tmp_name"];
    if( !file_exists($filename) )
        return "";

    // Controlla che sia stata caricata un'immagine
    $type = $file["type"];
    if( preg_match( "#^image/#", $type ) == false )
        return "<p>Tipo MIME dell'immagine non riconosciuto: «${type}».</p>";

    // Ridimensiona, se necessario, le immagini troppo grandi
    exec( "convert $filename -resize \"150x150>\" $filename" );
    // Se le dimensioni in byte del file (_dopo_ la conversione) sono eccessive,
    // segnala l'errore. Questo è un limite di MySQL per i BLOB, quindi non è
    // arbitrario e va rispettato.
    if( filesize($filename) > 65537 )
        return "<p>File dell'immagine troppo grande, ".
               "anche dopo la riduzione a 150 pixel.</p>";

    $handle = fopen( $filename, "rb" );
    $data   = fread( $handle, filesize($filename) );
    fclose($handle);

    $query = "UPDATE translators SET tipofoto= ?, foto= ? WHERE email= ?";
    $stmt  = $db->prepare($query);
    $null = NULL;
    $stmt->bind_param('sbs', $type, $null, $email);
    $stmt->send_long_data(1, $data);
    $stmt->execute();
    $stmt->close();

    return "";
}

function randomPassword( $len = 8 )
{
    // Rimossi 0, O, l, I per evitare confusione con alcuni font
    $caratteri = '123456789'.
                 'abcdefghijkmnopqrstuvwxyz'.
                 'ABCDEFGHJKLMNPQRSTUVWXYZ'.
                 '!?#@%&()[]{}*/+-.,;:<>=_^';
    $password = '';
    for ($i = 0; $i < $len; $i++)
        $password .= $caratteri[rand(0, strlen($caratteri)-1)];
    return $password;
}

// Azzera la password di un utente identificato con mail
// Restituisce una descrizione di un eventuale errore, o stringa vuota se non
// c'è problema.
function resetPassword ($email, $db) {
    $password = randomPassword();
    $hashed = crypt($password);
    $query = "UPDATE translators SET password= ? WHERE email= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('ss', $hashed, $email);
    if ( $stmt->execute() ) {
        $messaggio = "La tua password è stata reimpostata correttamente, ".
                     "ed è ora $password. Puoi modificarla accedendo al sito.";
        inviaPosta( $email, "Reimpostazione password KDE-it", $messaggio );
    }
    return $db->error;
}

?>