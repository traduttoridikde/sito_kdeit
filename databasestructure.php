<?php
/**
*
* File per la generazione di un dump senza dati della banca dati.
*
*/

define('ASSOLI_SCRIPT', 1);

require_once( 'wallet.php' );

header("Content-Type: text/plain");
header("Content-Disposition: attachment; filename=struttura.sql");

exec("mysqldump -u$DB_user -p$DB_password --no-data i18n_it", $output);

foreach ( $output as $line ) {
    echo $line."\n";
}

?>