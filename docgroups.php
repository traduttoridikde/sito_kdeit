<?php
/**
 * File di visualizzazione delle statitiche raggruppate.
 */

define('ASSOLI_SCRIPT', 1);

include("header.php");
require_once( 'functions.php' );
require_once( 'configurazione.php' );
require_once( 'connessione.php' );
?>

<h1>Statistiche raggruppate della documentazione</h1>

<p>Sono qui elencate le statistiche per i file della documentazione,
ordinati per quantità di lavoro da fare decrescente. Le documentazioni sono
divise in più file (vedi colonna <strong>File</strong>): ognuno di essi file
avere un diverso traduttore responsabile.</p>
<p>Sono esclusi da queste statistiche i file <tt>man</tt>, che non
sono necessari per compilare la documentazione, e gruppi <tt>kcontrol</tt>
e <tt>kioslave</tt>, che vengono ricompilati individualmente.</p>

<table class="it_stat">
<tr>
<th>Gruppo</th>
<th>File</th>
<th>Gran totale</th>
<th>Grafico</th>
</tr>
<?php

$query = "SELECT SUBSTRING_INDEX(file, '_', 1) AS groupName, package, ".
         "COUNT(*) as number, SUM(done+fuzzy+todo) AS total, ".
         "SUM(done) AS done, SUM(fuzzy) AS fuzzy, SUM(todo) AS todo, ".
         "SUM(fuzzy+todo) AS workleft ".
         "FROM files NATURAL JOIN stats ".
         "WHERE type='DOC' AND file NOT LIKE '%_man%' AND ".
         "file NOT LIKE 'kcontrol_%' AND file NOT LIKE 'kioslave_%' ".
         "GROUP BY groupName HAVING number>1 ".
         "ORDER BY workleft DESC";
$grouped = $db->query($query);

while ( $g = $grouped->fetch_assoc() ) {
    $link="file.php?type=DOC&amp;status=unfinished&amp;package=".$g["package"];

    // Scrivi la riga della tabella
    echo "<tr>\n";
    echo "<td><a href=\"$link\">" . $g["groupName"] . "</a></td>\n";
    echo "<td style='text-align:right'>".$g["number"]."</td>" ;
    echo "<td style='text-align:right'>" . $g["total"] . "</td>\n";
    echo "<td style=\"text-align: center; \">";
    statPie( $g["done"], $g["fuzzy"], $g["todo"] );
    echo "</td>\n";
    echo "</tr>\n\n";
}
?>
</table>
<br/>

<?php include("footer.php"); ?>
