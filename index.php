<?php
/**
*
* File con il contenuto della home page
*
*/

define('ASSOLI_SCRIPT', 1);

include("header.php");

?>

<h1>Tradurre per KDE</h1>

<p style="text-align: right;">Questa <b>non</b> è la <a
href="https://kdeitalia.it/">pagina italiana di <b>KDE</b></a>, ma la pagina
del gruppo di traduzione in italiano.</p>

<p>KDE è una rete mondiale di ingegneri del software, artisti, scrittori,
traduttori e assistenti che si dedicano allo sviluppo di software libero.
Questa comunità ha creato centinaia di applicazioni libere tra cui
Frameworks, il desktop Plasma, il gruppo di programmi KDE Applications,
e tante altre.</p>
<p>Si tratta di <a href="https://www.gnu.org/philosophy/free-sw.it.html">software
libero</a>, sviluppato in origine su e per sistemi Linux e Unix,
ma sovente disponibile anche per altri sistemi operativi.</p>

<p>Il software KDE è anche tradotto in <a
href="https://l10n.kde.org/stats/gui/stable-kf5/team/">numerosissime lingue</a>,
e questa pagina è appunto dedicata al lavoro di traduzione di software KDE in italiano.
Lo sviluppo del progetto KDE si basa sui volontari, e per questo abbiamo sempre
bisogno di aiuto per migliorare la versione italiana di software KDE e mantenerne la
qualità. Se vuoi aiutarci, non avrai bisogno che di una buona conoscenza
dell'ortografia in italiano, una buona conoscenza (anche se solo "passiva")
dell'inglese, e buona volontà. Al momento abbiamo bisogno di aiuto soprattutto
per tradurre la documentazione, mentre le interfacce
(<acronym title="Guided User Interface">GUI</acronym>) sono per lo più complete.
Per maggiori informazioni su come diventare un traduttore,
<a href="nuovonuovo.php">fai clic qui</a>.</p>

<p>Tradurre per KDE non è solo un lavoro volontario: è
un'occasione per dare una ripassata alla propria ortografia (ehm...), imparare a
usare nuovi strumenti come Subversion o Git, che
possono poi finire per diventare utili anche sul tuo posto di lavoro, imparare a
lavorare in un gruppo sparso per l'Italia (e per il mondo), e farsi degli
amici.</p>

<p>Se sei incuriosito, vedi la guida per i nuovi traduttori
sul <a href="https://community.kde.org/KDE_Localization/it">nostro Wiki</a>.</p>

<table class="it_main">
<tr>
    <td colspan="2"><a href="nuovonuovo.php">
    <img src="images/lampo.png" alt="Nuovo traduttore" title="Nuovo traduttore"
         height="64px"/><br/>Guida Lampo per<br/>aspiranti traduttori<br/></a>
    </td>
    <td colspan="2"><a href="pacchetti.php">
    <img src="images/pacchi.png" alt="File e pacchetti" title="File e pacchetti"
         height="64px"/><br/>File e pacchetti<br/></a>
    </td>
    <td colspan="2"><a href="https://community.kde.org/KDE_Localization/it">
    <img width="64" src="images/wiki.png" alt="Wiki e Glossario"
         title="Wiki e Glossario" height="64px"/><br/>Wiki e Glossario<br/></a>
    </td>
</tr>
<tr>
    <td colspan="3"><a href="stat.php">
    <img width="64" src="images/stat.png" alt="Statistiche" title="Statistiche"
         height="64px"/><br/>Statistiche<br/></a>
    </td>
    <td colspan="3"><a href="ricerca.php">
    <img width="64" src="images/trova.png" alt="Ricerca" title="Ricerca"
         height="64px"/><br/>Cerca traduzioni<br/></a>
    </td>
</tr>
</table>

<?php

include("footer.php");

?>
