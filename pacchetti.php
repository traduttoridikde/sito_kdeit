<?php
/**
 * Elenco dei pacchetti e collegamenti agli elenchi dei loro file.
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
require_once( 'functions.php' );

$PHP_SELF = $_SERVER['PHP_SELF'];

include("header.php");

?>

<h1>Pacchetti e responsabili</h1>

<table class="it_pack it_centrata">
<tr>
<th>Pacchetto</th>
<th>GUI</th>
<th>DOC</th>
<th>Responsabile</th>
</tr>

<?php
    $query = "SELECT package, idTranslator, ".
                    "CONCAT_WS(' ', first, last) AS name, ".
                    "SUM(IF(type='GUI', done, 0)) AS GUIdone, ".
                    "SUM(IF(type='GUI', fuzzy, 0)) AS GUIfuzzy, ".
                    "SUM(IF(type='GUI', todo, 0)) AS GUItodo, ".
                    "SUM(IF(type='DOC', done, 0)) AS DOCdone, ".
                    "SUM(IF(type='DOC', fuzzy, 0)) AS DOCfuzzy, ".
                    "SUM(IF(type='DOC', todo, 0)) AS DOCtodo ".
             "FROM packages NATURAL JOIN stats NATURAL LEFT JOIN translators ".
             "GROUP BY package ORDER BY package ASC";
    $results = $db->query($query);

    while ( $result = $results->fetch_assoc() ) {
        $package = $result["package"];
        $id      = $result["idTranslator"];
        $name    = $result["name"];

        echo "<tr>\n";
        echo "<td><a href=\"file.php?package=$package\">";
        echo "<b>$package</b></a></td>\n";

        echo "<td style='text-align: center;'>";
        statPie( $result["GUIdone"], $result["GUIfuzzy"], $result["GUItodo"] );
        echo "</td>\n";

        echo "<td style='text-align: center;'>";
        statPie( $result["DOCdone"], $result["DOCfuzzy"], $result["DOCtodo"]);
        echo "</td>\n";

        if ($name)
            echo "<td><a href=\"traduttori.php#id-$id\">$name</a></td>\n";
        else
            echo "<td>Non assegnato</td>\n";

        echo "</tr>\n\n";
    }
    echo "</table>\n";


include("footer.php");

?>
