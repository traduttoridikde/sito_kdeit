<?php
/**
 * File che genera le statistiche per i pacchetti GUI e DOC
 */

define('ASSOLI_SCRIPT', 1);

include("header.php");

?>

<h1>Statistiche utili</h1>
<ul>
<li><a href="file.php?status=unstarted">Pacchetti senza traduzione</a>
(<b>Attenzione</b>: sono comunque assegnati a qualcuno: <em>chiedere prima di
tradurre</em>!)</li>
<!-- FIXME Questo va aggiornato quando so quali pacchetti vanno in quale raccolta
<li><a href="file.php?collection=Plasma">File
dell'interfaccia di KDE</a>: tutto lavoro da fare per completare i
file dell'interfaccia.</li>
-->
<li><a href="file.php?translator=-1">File non assegnati a nessun traduttore</a>.
Questi file possono venire assegnati d'ufficio a chi li richiede.</li>
<li><a href="doppioni.php">File con lo stesso nome</a>, che potrebbero
essere stati spostati di recente.</li>
<li><a href="docgroups.php">Statistiche raggruppate delle
documentazioni</a>, anche quelle divise in più file.</li>
</ul>

<?php

include("footer.php");

?>
