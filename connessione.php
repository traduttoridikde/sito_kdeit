<?php
/**
 * Connessione alla banca dati.
 */

// Controllo che impedisce il richiamo diretto del file include dall'esterno.
if( !defined('ASSOLI_SCRIPT') )
{
     header('HTTP/1.1 404 Not Found');
?>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL <?php $_SERVER['REQUEST_URI'] ?> was not found on this server.</p>
<hr>
<address><?php $_SERVER['SERVER_SIGNATURE'] ?> Server at kdeit.softwarelibero.it Port 80</address>
</body></html>
<?php
    exit;
}

$db = new mysqli($DB_host, $DB_user, $DB_password, $DB_nomedb);

if ($db->connect_errno)
    exit ( 'Impossibile connettersi alla banca dati: '.
           htmlspecialchars($db->connect_error) );

$db->set_charset('utf8');

?>
