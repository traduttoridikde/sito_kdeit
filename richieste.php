<?php
/**
 * Gestione delle richieste di assegnazione.
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );

include( "identifica.php" ) ;
include( "header.php" );
include( "functions.php" );

$translations = chkvar( $_POST['translations'] );
$reviews      = chkvar( $_POST['reviews'] );
$comment      = chkvar( $_POST['comment'] );

$requests = array();
foreach ($translations as $translation)
    $requests[] = "T/$translation";
foreach ($reviews as $review)
    $requests[] = "R/$review";
unset( $translations, $reviews );

$query = "SELECT idTranslator, CONCAT_WS(' ', first, last) AS name ".
         "FROM translators WHERE email= ?";
$stmt  = $db->prepare($query);
$stmt->bind_param('s', $_SERVER['PHP_AUTH_USER']);
$stmt->execute();
$stmt->bind_result($id, $name);
$stmt->fetch();
$stmt->close();

$owners = array();

foreach ( $requests as $request ) {

    list( $task, $type, $package, $file ) = explode( '/', $request );
    $filename = substr( $request, 2 );

    $isReviewCond = ($task == 'R');
    $ownerId = findOwner( $type, $package, $file, $isReviewCond, $db );
    $isReview = ($isReviewCond ? 'TRUE' : 'FALSE');

    if ( $ownerId == NULL || $ownerId == $id ) { // Da|a non assegnati
        $field = $ownerId ? 'idTranslator' : 'idFrom';
        $query = "INSERT INTO requests SET $field= ?, since=NOW(), ".
                 "file= ?, package= ?, type= ?, review= ?";
        $stmt  = $db->prepare($query);
        $stmt->bind_param('isssi', $id, $file, $package, $type, $isReview);
        $stmt->execute();
        $stmt->close();
        $idRequest = $db->insert_id;
        handleRequest( $idRequest, TRUE, $db );
        $action = $ownerId ? "abbandonato" : "assegnato d'ufficio";
        echo "<p>$filename: $action.</p>\n";
    } else { // Richiesta ad altro traduttore
        $query = "INSERT INTO requests SET idTranslator= ?, idFrom= ?, ".
                 "file= ?, package= ?, type= ?, review= ?, since=NOW()";
        $stmt  = $db->prepare($query);
        $stmt->bind_param('iisssi',$ownerId,$id,$file,$package,$type,$isReview);
        $stmt->execute();
        $stmt->close();
        $owners[] = $ownerId;
        echo "<p>$filename: richiesta inoltrata.</p>\n";
    }
}

$url     = "http://kdeit.softwarelibero.it/decidi.php";
$subject = "Richiesta di acquisizione di un file";
$message = "(Messaggio autogenerato)\n".
           "$name ha chiesto di poter tradurre o rileggere alcuni file ".
           "attualmente sotto la tua responsabilità. Decidi se autorizzare ".
           "o meno le sue richieste visitando\n$url\n\n";
if ( $comment )
    $message .= "Messaggio da parte di $name:\n\n".$comment;
$message = wordwrap( stripslashes( $message ) );

$owners = array_unique( $owners );
foreach ( $owners as $owner ) { // Invia gli avvisi
    $query = "SELECT email FROM translators WHERE id=$owner";
    $result = $db->query($query);
    $ownerMail = $result->fetch_object()->email;
    inviaPosta($ownerMail, $subject, $message, $_SERVER['PHP_AUTH_USER']) ;
}

include("footer.php");

?>
