<?php
/**
 * File per cercare traduzioni specifiche nella banca dati.
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
require_once( 'functions.php' );

include("header.php");

// Evita manualmente iniezioni di codice; non si può usare mysqli::prepare
// con un numero variabile di argomenti
$search     = chkvar( $_GET["search"], false);
$filename   = $db->escape_string( chkvar( $_GET["filename"] ) );
$translator = $db->escape_string( chkvar( $_GET["translator"] ) );
$msgid      = $db->escape_string( chkvar( $_GET["msgid"] ) );
$msgstr     = $db->escape_string( chkvar( $_GET["msgstr"] ) );
$msgidre    = chkvar( $_GET["msgidre"] );
$msgstrre   = chkvar( $_GET["msgstrre"] );
$page       = chkvar( $_GET["page"], 1);
$GUI        = chkvar( $_GET["GUI"], false);
$DOC        = chkvar( $_GET["DOC"], false);
$noTag      = chkvar( $_GET["noTag"], false);
// Almeno 2 risultati per pagina; se qualcuno dà <= 1 viene filtrato qui.
$perpage    = $db->escape_string( max( 2, chkvar($_GET["perpage"], 10) ) );

// Cinque barre verticali vengono usate per separare le forme plurali.
// Se ce ne sono, vanno sostituite con un <br/> per la visualizzazione.
function parseMsg( $msg ) {
    return str_replace( "|||||", "<br/>",
                        htmlspecialchars( $msg, ENT_QUOTES, "UTF-8" ) );
}

// Se nessuno dei due è stato selezionato, selezionali entrambi
if( !$GUI && !$DOC ) {
    $GUI = TRUE;
    $DOC = TRUE;
}

if( $search ) {
    $messages = $noTag ? "msgidnotag AS msgid, msgstrnotag AS msgstr" :
                         "msgid, msgstr";
    $WHERES = array();
    if( !empty($filename) )
        $WHERES[] = "(file LIKE '%$filename%' OR package LIKE '%$filename%')";
    if( !empty($translator) )
        $WHERES[] = "idTranslator='$translator'";
    if( !empty($msgid) ) {
        $column   = $noTag   ? "msgidnotag" : "msgid";
        $WHERES[] = $msgidre ? "$column REGEXP '$msgid'" :
                               "$column  LIKE '%$msgid%'";
    }
    if( !empty($msgstr) ) {
        $column   = $noTag    ? "msgstrnotag" : "msgstr";
        $WHERES[] = $msgstrre ? "$column REGEXP '$msgstr'" :
                                "$column  LIKE '%$msgstr%'";
    }
    if( !$GUI )
        $WHERES[] = "type='DOC'";
    if( !$DOC )
        $WHERES[] = "type='GUI'";

    $WHERE = empty($WHERES) ? "" : "WHERE ".implode( " AND ", $WHERES );

    $start = ($page-1)*$perpage;
    $query = "SELECT file, package, type, number, isFuzzy, msgctxt, ".
             "$messages, CONCAT_WS(' ', first, last) AS name, ".
             "idTranslator ".
             "FROM translations NATURAL JOIN files ".
             "NATURAL LEFT JOIN translators ".
             "$WHERE ORDER BY package, file, number, type ".
             "LIMIT $start, $perpage";

    $results = $db->query($query);

    echo "<h1>Risultato della ricerca</h1>\n";
    if( $results->num_rows > 0 ) {
?>
        <table class="it_stat it_centrata">
        <tr>
        <th>File<br/>Pacchetto<br/>Tipo<br/>Traduttore</th>
        <th>Nº</th>
        <th>Originale</th>
        <th>Traduzione</th>
        <th>Contesto</th>
        </tr>
<?php
        while( $row = $results->fetch_assoc() ) {
            echo $row["isFuzzy"] ? "<tr class='fuzzy'>\n" : "<tr>\n";
            echo "<td style='text-align: center; white-space: nowrap;'>";
            echo "<a href='file.php?package=".$row['package']."'>".
                 $row['package']."</a><br/>";
            echo $row['file']."<br/>";
            echo "<a href='file.php?package=".$row['package']."&amp;type=".
                 $row['type']."'>".$row['type']."</a><br/>";
            if ($row['idTranslator'])
                echo "<a href='traduttori.php#id-".$row['idTranslator']."'>".
                     $row['name']."</a>";
            else
                echo "Non assegnato";
            echo "</td>\n";
            echo "<td>".$row["number"]."</td>\n";
            echo "<td>".parseMsg($row["msgid"])."</td>\n";
            echo "<td>".parseMsg($row["msgstr"])."</td>\n";
            echo "<td>".parseMsg($row["msgctxt"])."</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n\n";

        $totals = $db->query("SELECT COUNT(*) AS C FROM translations $WHERE");
        $number = $totals->fetch_field()->C;

        pageIndex($page, $number, $perpage);
    } else {
?>
        <p>Nessun risultato per la ricerca nella banca dati. Se hai usato le
        espressioni regolari, può darsi che non ti sia accorto che le
        espressioni regolari di MySQL sono un po' diverse dal solito. Vedi la <a
        href="http://dev.mysql.com/doc/refman/5.5/en/regexp.html">documentazione
        di MySQL</a>.</p>

<?php
    }
} else {
?>

    <h1>Ricerca</h1>

    <p>Da questa pagina potete cercare testi originali, traduzioni e file
    attualmente presenti nel deposito di Subversion di KDE per la lingua
    italiana. La banca dati delle traduzioni è aggiornata insieme alle
    statistiche, quindi le modifiche su Subversion <em>non</em> si riflettono
    immediatamente qui.</p>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
    <table class="it_centrata">
    <tr>
    <td>File o del pacchetto</td>
    <td>
<?php
    echo "<input type='text' name='filename'";
    if( isset($filename) )
        echo " value='".htmlspecialchars( stripslashes($filename) )."'";
    echo "/>\n";
?>
    </td>
    <td>Anche solo una parte del nome</td>
    </tr>
    <tr>
    <td>Tra i file del traduttore:</td>
    <td><select name="translator">
<?php
    echo "<option ";
    if( !isset($translator) )
        echo "selected='selected' ";
    echo "value=''>Tutti</option>\n";

    $query = "SELECT CONCAT_WS(' ', first, last) AS name, idTranslator ".
             "FROM translators ORDER BY name ASC";
    $result = $db->query($query);
    while( $people = $result->fetch_assoc() ) {
        $id   = $people["idTranslator"];
        $nome = $people["name"];
        echo "<option ";
        if( $translator == $id )
            echo "selected='selected' ";
        echo "value='$id'>$nome</option>\n";
    }
?>
    </select>
    </td>
    </tr>
    <tr>
    <td><img src='images/us.svg' alt='Inglese' style='max-height: 30px;'/>
    Originale:</td>
    <td>
<?php
    echo "<input type='text' name='msgid'";
    if( isset($msgid) )
        echo " value='".htmlspecialchars( stripslashes($msgid) )."' ";
    echo "/>\n";
?>
    </td>
    <td>
<?php
    echo "<input type='checkbox' value='true' name='msgidre'";
    if( $msgidre )
        echo " checked='checked'";
    echo "/>";
?>
    Usa le <a href="http://dev.mysql.com/doc/refman/5.5/en/regexp.html">
    espressioni regolari</a>
    </td>
    </tr>
    <tr>
    <td><img src='images/it.svg' alt='Italiano' style='max-height: 30px;'/>
    Traduzione:</td>
    <td>
<?php
    echo "<input type='text' name='msgstr'";
    if( isset($msgstr) )
        echo " value='".htmlspecialchars( stripslashes($msgstr) )."' ";
    echo "/>\n";
?>
    </td>
    <td>
<?php
    echo "<input type='checkbox' value='true' name='msgstrre'";
    if( $msgstrre )
        echo " checked='checked'";
    echo "/>";
?>
    Usa le <a href="http://dev.mysql.com/doc/refman/5.5/en/regexp.html">
    espressioni regolari</a>
    </td>
    </tr>
    <tr>
    <td rowspan="2" style='vertical-align:middle;'>Ricerca nelle:</td>
    <td>
<?php
    echo "<input type='checkbox' value='true' name='GUI'";
    if($GUI)
        echo " checked='checked'";
    echo "/>\n";
?>
    interfacce</td>
    <td rowspan="2" style='vertical-align:middle;'>
<?php
    echo "<input type='checkbox' value='true' name='noTag'";
    if($noTag)
        echo " checked='checked'";
    echo "/>\n";
?>
    Ignora i &lt;tag&gt;</td>
    </tr>
    <tr>
    <td>
<?php
    echo "<input type='checkbox' value='true' name='DOC'";
    if($DOC)
        echo " checked='checked'";
    echo "/>\n";
?>
    documentazioni</td>
    </tr>
    <tr>
    <td colspan="3" style="text-align:center">
    <button name="search" value="true">Cerca!</button></td>
    </tr>
    </table>
    </form>

    <p>Notate che le espressioni regolari non seguono la convenzione che vi
    aspettereste, ma <a
    href="http://dev.mysql.com/doc/refman/5.5/en/regexp.html"> quella di
    MySQL</a>.
    Se non usate le espressioni regolari, verrà usato
    <a href="http://dev.mysql.com/doc/refman/5.5/en/pattern-matching.html">
    l'operatore <tt>LIKE</tt></a>, e il vostro parametro di ricerca avrà due
    segni di percentuale (caratteri jolly di MySQL) su ambo i lati:
    <tt>pippo</tt> diventa <tt>LIKE '%pippo%'</tt>.
    Notate anche che le lettere accentate si possono distinguere <em>solo</em>
    usando le espressioni regolari.</p>

<?php
}
include("footer.php");
?>