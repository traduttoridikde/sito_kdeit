<?php
/**
*
* File di introduzione per i nuovi traduttori.
*
*/

define('ASSOLI_SCRIPT', 1);

include("header.php");
?>

<h1>Guida lampo per i nuovi traduttori</h1>

<p>Per prima cosa <strong>benvenuto</strong>, abbiamo bisogno del tuo aiuto! 
Questa è una brevissima guida a cosa fare per cominciare a tradurre per KDE.</p>

<ul>
<li>Iscriviti alla nostra <a href="https://mail.kde.org/mailman/listinfo/kde-i18n-it">lista
di distribuzione</a>, se vuoi manda un messaggio di presentazione;</li>
<li><a href="register.php">Iscriviti a questo sito</a> per
poterti far assegnare i file da tradurre;</li>
<li>Dalla <a href="pacchetti.php">pagina dei pacchetti</a>,
seleziona da quale pacchetto vuoi prendere un file. Dài un'occhiata soprattutto ai playground
in basso a sinistra, sono ideali per cominciare.</li>
<li>Scegli un file dall'elenco che viene fuori facendo clic su un pacchetto.
Dovresti sceglierne uno non troppo grande, diciamo con meno di 100 messaggi.
Per richiederlo, spunta la colonna <em>Trad</em> sulla destra (la prima), vai in fondo alla
pagina e premi <em>Richiedi</em>.</li>
<li>Adesso devi aspettare che l'attuale responsabile del file ti dia il via libera. Hai
qualche ora di tempo per <a href="https://community.kde.org/KDE_Localization/it">leggere il
nostro wiki</a>.</li>
<li>Se il responsabile tarda a darti via libera, magari è in vacanza o altrimenti
impossibilitato a rispondere. Segnala la cosa in lista, oppure scegli un altro file di
un altro responsabile.</li>
</ul>

<?php
include("footer.php");
?>

