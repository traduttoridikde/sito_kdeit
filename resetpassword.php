<?php
/**
 * Generazione e invio di nuova password generata casualmente
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
require_once( 'func_reg.php' );

// Inizializzazione variabili e controllo
$email = chkvar( $_POST['email'] );

include( 'header.php' );

?>

<div class="it_trad">
<h2 style="color:white">Dimenticato la password?</h2>

<?php

if ( !empty($email) ) {
    // Richiedi nuova password per l'indirizzo dato
    $error = resetPassword( $email, $db );
    if ($error)
        echo "<p>Qualcosa è andato storto. Questo è quello di cui si lamenta ".
             "la banca dati:</p>\n<p><tt>$error</tt></p>\n";
    else
        echo "<p>Tutto bene! La password è stata reimpostata, e la nuova ".
             "password è stata inviata al tuo indirizzo <tt>$email</tt>.</p>";

} else {

?>

<p>Se ti sei dimenticato la tua password, il sistema la può reimpostare da qui.
Scrivi il tuo indirizzo di posta elettronica (quello che hai usato per
iscriverti) e premi il pulsante per ricevere una nuova password.</p>

<p style="text-align:center">
<form method="post" enctype="multipart/form-data" action="resetpassword.php">
<input type="text" name="email"/><br/>
<button type="submit" name="submit" value="true">Richiedi password</button>
</form>
</p>

<p>Se hai perso l'accesso all'indirizzo di posta che hai usato finora, devi
chiedere aiuto agli amministratori del sito nella
<a href="mailto:kde-i18n-it@kde.org">lista</a>.</p>

<p>Se hai usato un indirizzo non valido per aprire un account, devi
<a href="register.php">aprirne uno con un indirizzo valido</a>.</p>

<?php
}

echo "</div>\n";

include("footer.php");

?>
