﻿<?php
/**
*
* File di presentazione del sistema di sviluppo del sito.
*
*/

define('ASSOLI_SCRIPT', 1);

include("header.php");
?>

<h1>
<img src="https://www.gnu.org/graphics/agplv3-155x51.png" alt="Codice
         del sito sotto licenza GNU Affero 3.0" style="float:right;"/>
Codice del sito</h1>

<p>Le nostre pagine Web, e il codice che le generano, sono disponibili secondo
la licenza <a href="http://www.gnu.org/licenses/agpl-3.0.html">GNU Affero 
3.0</a>. Tutti possono scaricare il codice, e sono i benvenuti a 
migliorarlo.</p>

<p>Il nostro sito si basa su delle pagine <acronym>XHTML</acronym> generate da
PHP, una banca dati MySQL e un server Apache.
Il codice sorgente è scaricabile attraverso 
<a href="https://bitbucket.org/traduttoridikde">due depositi Git su 
BitBucket</a>, uno per le 
<a href="https://bitbucket.org/traduttoridikde/sito_kdeit">pagine Web</a> e uno
per il
<a href="https://bitbucket.org/traduttoridikde/aggiornamento_kdeit">sistema di 
generazione e aggiornamento delle statistiche</a>.</p>

<p>Se vuoi solo scaricare il codice del sito, basta clonare questi depositi;
se vuoi dare una mano, ti sarà utile mettere in piedi un «sito locale».</p>


<h2>Installazione locale del sito</h2>
<p>Per installare localmente il sito ti è necessario innanzi tutto installare
Apache 2.x, MySQL 5.x e PHP; se non viene fatto automaticamente dal tuo
sistema di gestione dei pacchetti, è necessario installare l'estensione MySQL
per PHP.</p>

<p>Nel seguito viene spiegato come installare una copia locale del sito su
un sistema Kubuntu; nota che i percorsi indicati, sul tuo sistema, potrebbero
essere leggermente diversi.</p>


<h3>Configurazione di Apache</h3>
<p>Dopo aver installato Apache, clona il <a href="hg/hgwebdir.cgi/sito/">
deposito del sito</a> in una sottocartella di <tt>/var/www</tt>, per esempio
<tt>/var/www/kde</tt>.
Configura quindi Apache per riconoscere il sito: supponendo che tu stia usando
una distribuzione basata su Debian, come Kubuntu, aggiungi il file <tt>kde</tt> 
nella cartella <tt>/etc/apache2/sites-available</tt>, contenente:</p>

<pre>&lt;VirtualHost *:80&gt;
        ServerName kde-test
        DocumentRoot /var/www/kde
        &lt;Directory /&gt;
                AllowOverride All
                Order deny,allow
                Allow from all
        &lt;/Directory&gt;
&lt;/VirtualHost&gt;</pre>

<p>Fai quindi un collegamento simbolico da <tt>sites-available/kde</tt> a
<tt>sites-enabled/kde</tt>, e riavvia Apache per fargli leggere la nuova
configurazione. Aggiungi inoltre in <tt>/etc/hosts</tt> la riga:</p>

<pre>127.0.0.1       kde-test</pre>

<p>Dovresti a questo punto poter navigare sulla copia locale del sito da <a 
href="http://kde-test">questo collegamento</a>. La pagina principale dovrebbe
funzionare, ma tutte quelle che richiedono accesso alla banca dati (come 
<a href="http://kde-test/pacchetti.php"><tt>pacchetti.php</tt></a>) non 
funzioneranno ancora.</p>


<h3>Configurazione di MySQL</h3>

<p>La configurazione di MySQL è un po' più complicata. Innanzi tutto dovrai
creare una banca dati, un utente con password e dargli accesso alla banca dati.
Collegati quindi a mysql come utente root (nota che l'utente root di MySQL non
ha <em>niente</em> a che fare con l'utente root del sistema!), e digita i
comandi:</p>

<pre>CREATE DATABASE i18n_it;
CREATE USER utente@localhost IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON i18n_it.* TO utente;
</pre>

<p>Dove <tt>utente</tt> e <tt>password</tt> possono essere quello che vuoi,
basta che mantieni la consistenza nel seguito.</p>

<p>Rimane da copiare la struttura del database del sito: questa viene a volte
modificata (per esempio quando abbiamo passato la soglia dei 256 iscritti...),
e puoi scaricare quella attuale <a href="databasestructure.php">da qui</a>.</p>

<p>Il file non contiene nessun dato (sia per sicurezza che per ridurre le
dimensioni del file da scaricare), e per importare la struttura devi
eseguire:</p>

<pre>mysql i18n_it &lt; struttura.sql -uutente -ppassword</pre>


<h3>Indicare al sistema il nome utente e la password per la banca dati</h3>

<p>Il sistema ha bisogno di sapere come identificarsi rispetto alla banda dati
MySQL per chiedere accesso alle sue tabelle.
Bisogna pertanto creare un file, chiamato <tt>wallet.php</tt>, da inserire
<em>sia</em> nella copia locale del sito <em>che</em> del sistema di
aggiornamento:</p>
<pre>&lt;?php

$DB_user     = "utente";
$DB_password = "password";

?&gt;
</pre>

<p>Inoltre, nel sistema di aggiornamento, va messo un ulteriore file,
<tt>wallet</tt> (senza estensione <tt>.php</tt>):</p>
<pre>DB_USERNAME="utente"
DB_PASSWORD="password"
</pre>

<p>Questi file <strong>non vanno mai caricati sui depositi</strong>, per motivi
di sicurezza; il deposito sul sito centrale, ovviamente, ha le sue copie locali
con il nome utente e la password usate dal sito.</p>


<h3>Avviare il sistema</h3>

<p>Se vuoi inserire nel sistema delle statistiche, per vedere come funzionano le
tue modifiche, devi prima andare nella cartella di aggiornamento ed eseguire,
solo la prima volta, lo script <tt>ritiraDeposito</tt>.
Ci vorrà un po' per scaricare tutte le traduzioni italiane di KDE.</p>

<p>Una volta che i file sono stati scaricati, potrai avviare lo script di
aggiornamento, <tt>update-stats</tt>. Anche questo ci metterà il suo tempo,
ma puoi velocizzarne significativamente l'esecuzione commentando la riga che
richiama <tt>inserisciTraduzioni</tt>, l'istruzione più pesante perché estrae
tutte le ultime traduzioni e le inserisce nella banca dati.</p>


<h2>Contribuire il tuo codice</h2>

<p>Se hai fatto dei miglioramenti e vuoi introdurli nel codice del sito, ma non
hai accesso in scrittura ai depositi centrali, contatta la <a
href="https://mail.kde.org/mailman/listinfo/kde-i18n-it">mailing list</a> del
gruppo di traduzione.</p>

<p>Nota che se le tue modifiche richiedono di cambiare la struttura della banca
dati (aggiungere colonne, cambiare tipi di dati, o altro), questo va fatto a
mano da un amministratore; ricordati di segnalarlo prima di depositare le tue
modifiche sul server centrale!</p>

<?php
include("footer.php");
?>
