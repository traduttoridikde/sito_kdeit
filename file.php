<?php
/**
 * Presentazione delle statistiche e dei responsabili dei file di un dato
 * pacchetto.
 * Da questa pagina è possibile
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
require_once( 'functions.php' );

// Evita manualmente iniezioni di codice; non si può usare mysqli::prepare
// con un numero variabile di argomenti
$collection = $db->escape_string( chkvar( $_GET['collection'] ) );
$package    = $db->escape_string( chkvar( $_GET['package'] ) );
$reviewer   = $db->escape_string( chkvar( $_GET['reviewer'] ) );
$status     = $db->escape_string( chkvar( $_GET['status'] ) );
$translator = $db->escape_string( chkvar( $_GET['translator'] ) );
$type       = $db->escape_string( chkvar( $_GET['type'] ) );
$ORDER      = $db->escape_string( chkvar( $_GET['order'], 'file ASC' ) );

include("header.php");
$WHERES   = array();
$options  = array();
$criteria = array();

if ($collection) {
    $WHERES[]   = "collection='$collection'";
    $criteria[] = "File della raccolta <strong>$collection</strong>";
}

if ($package) {
    $WHERES[]   = "package='$package'";
    $criteria[] = "File del pacchetto <strong>$package</strong>";
    $options[]  = 'nopackage';
}

if ($translator) {
    $name       = getName( $translator, $db );
    $WHERES[]   = 'files.idTranslator'.($name?"=$translator":' IS NULL');
    $criteria[] = ($name) ? "File assegnati a <strong>$name</strong>" :
                            'File non assegnati';
    $options[]  = 'notranslator';
}

if ($reviewer) {
    $name       = getName( $reviewer, $db );
    $WHERES[]   = 'idReviewer'.($name?"=$reviewer":' IS NULL');
    $criteria[] = ($name) ? "File sotto revisione di <strong>$name</strong>" :
                            'File senza revisore';
    $options[]  = 'noreviewer';
}

if ($status == 'unfinished') {
    $WHERES[]   = 'fuzzy+todo>0';
    $criteria[] = 'File non completati';
    $ORDER      = "tr_name ASC, total-done DESC";
} elseif ($status == 'unstarted') {
    $WHERES[]   = 'fuzzy+done=0';
    $criteria[] = 'File senza traduzione';
    $options[]  = 'nostatgraph';
}

if ($type) {
    $WHERES[]   = "type='$type'";
    $criteria[] = ($type == 'GUI') ? "File dell'interfaccia" :
                                     "File della documentazione";
    $options[]  = 'notype';
}
statTable($db, implode(' AND ', $WHERES), $ORDER, $options, $criteria);

include("footer.php");

?>
