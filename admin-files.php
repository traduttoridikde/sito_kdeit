<?php
/**
 * Amministrazione dei file, cioè assegnazione manuale di traduttore e revisore
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
include('functions.php');
include('checkadmin.php') ;

// Controllo e inizializzazione variabili
$package = chkvar(  $_GET['package'] );
$actions = chkvar( $_POST['actions'], array() );

include_once('header.php');

// Presentazione del pacchetto richiesto
if( $package ) {
    echo "<h1>File del pacchetto $package</h1>\n";
?>
<p>I file sono preimpostati con il loro traduttore e revisore attuali.</p>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<table class="it_pack it_centrata">
<tr>
<th>File</th>
<th>Tipo</th>
<th>Traduttore</th>
<th>Revisore</th>
</tr>
<?php
    $query = "SELECT idTranslator, CONCAT_WS(' ', first, last) AS name ".
             'FROM translators ORDER BY name';
    $result = $db->query($query);
    $translators = array();
    $translators[NULL] = 'Non assegnato';
    while( $r = $result->fetch_assoc() )
        $translators[$r['idTranslator']] = $r['name'];

    $query = 'SELECT file, type, idTranslator, idReviewer FROM files '.
             "WHERE package= ? ORDER BY type, file";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('s', $package);
    $stmt->execute();
    $stmt->bind_result($file, $type, $tId, $rId);
    while( $stmt->fetch() ) {
        echo "<tr>\n";
        echo "<td>$file</td>\n";
        echo "<td>$type</td>\n";
        echo "<td><select name='actions[]'>\n";
        foreach( $translators as $id => $name ) {
            echo "\t<option ";
            echo ( $id == $tId ) ? "value='' selected='TRUE'>" :
                                   "value='0/$type/$package/$file/$id'>";
            echo "$name</option>";
        }
        echo "</select></td>\n";
        echo "<td><select name='actions[]'>\n";
        foreach( $translators as $id => $name ) {
            echo "\t<option ";
            echo ( $id == $rId ) ? "value='' selected='TRUE'>" :
                                   "value='1/$type/$package/$file/$id'>";
            echo "$name</option>";
        }
        echo "</select></td>\n";
        echo "</tr>\n";
    }
?>
</table>

<div style="text-align:center; padding: 2ex;">
<button type="submit" name="editfiles" value="true"
        onclick="return confirm('Sei sicuro?')"
        style="padding: 1ex;">Applica modifiche</button>
</div>
</form>

<?php

}

// Implementazione delle modifiche richieste
foreach( $actions as $action ) {
    if( !$action  )
        continue;
    list( $isReview, $type, $package, $file, $newID ) = explode( '/', $action );
    //$newID = $newID ? $newID : 'NULL';

    // Trasferisci file (traduzione o revisione)
    $role  = $isReview ? 'idReviewer' : 'idTranslator';
    $query = "UPDATE files SET $role= ? ".
             'WHERE file= ? AND package= ? AND type= ?';
    $stmt  = $db->prepare($query);
    $stmt->bind_param('isss', $newID, $file, $package, $type);
    $stmt->execute();

    // Inoltra richieste pendenti al nuovo responsabile
    $query = 'UPDATE requests SET idTranslator= ? '.
             'WHERE file= ? AND package= ? AND type= ? AND review= ? '.
             'AND decision IS NULL';
    $stmt  = $db->prepare($query);
    $stmt->bind_param('isssi', $newID, $file, $package, $type, $isReview);
    $stmt->execute();
}

?>

<h1>Seleziona il pacchetto</h1>
<p>Scegli il pacchetto di cui vuoi modificare i file.
Potrai modificare il traduttore e il revisore di ogni file.</p>

<table class="it_pack it_centrata" style="margin-bottom: 2ex">
<tr><th>Pacchetto</th></tr>

<?php
$packages = $db->query('SELECT package FROM packages ORDER BY package');
while( $r = $packages->fetch_assoc() ) {
    $package = $r['package'];
    echo '<tr><td>';
    echo '<a href="'.$_SERVER['PHP_SELF']."?package=$package\">$package</a>";
    echo "</td></tr>\n";
}
echo "</table>\n";

include("footer.php");
?>
