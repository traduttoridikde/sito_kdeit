<?php
/**
 * Funzioni generali del sito, usate in più file.
 */

// Controllo che impedisce il richiamo diretto del file include dall'esterno.
if( !defined('ASSOLI_SCRIPT') )
 {
     header('HTTP/1.1 404 Not Found');
?>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL <?php $_SERVER['REQUEST_URI'] ?> was not found on this server.</p>
<hr>
<address><?php $_SERVER['SERVER_SIGNATURE'] ?> Server at kdeit.softwarelibero.it Port 80</address>
</body></html>
<?php
    exit;
}

require_once( 'configurazione.php' );

// Stampa le scadenze non ancora passate
function deadlines( $db ) {
    $query = "SELECT name, date FROM deadlines ".
             "WHERE date > CURDATE() ORDER BY date ASC" ;
    $deadlines = $db->query($query) ;

    if ( $deadlines->num_rows == 0 )
        return;

    echo "<fieldset class=\"it_feedback\">\n";
    echo "<legend>Prossime scadenze</legend>\n<ul>\n";
    while ( $deadline = $deadlines->fetch_assoc() ) {
        $date = ucfirst( strftime("%A %e %B %Y",
                                  strtotime( $deadline["date"] ) ) );
        echo "<li><strong>$date</strong>: ".$deadline["name"].".</li>\n";
    }
    $deadlines->free();
    echo "</ul></fieldset>\n";
}

function create_calendar( $db, $filename ) {
    $query = "SELECT name, date FROM deadlines ".
             "WHERE date > CURDATE() ORDER BY date ASC";
    $deadlines = $db->query($query);

    $handle = fopen( $filename, "w" );
    if( !$handle )
        return;

    fwrite( $handle, "BEGIN:VCALENDAR\r\nVERSION:2.0\r\n");
    fwrite( $handle, "PRODID:-//KDE/i18n-it/CAL\r\n" );
    while ( $deadline = $deadlines->fetch_assoc() ) {
        fwrite( $handle, "BEGIN:VEVENT\r\n" );
        $date = strftime('%Y%m%d', strtotime( $deadline['date'] ) );
        fwrite( $handle, "DTSTART:".$date."T000000Z\r\n" );
        fwrite( $handle, "SUMMARY:".$deadline['name']."\r\n" );
        fwrite( $handle, "END:VEVENT\r\n" );
    }
    $deadlines->free();
    fwrite( $handle, "END:VCALENDAR\r\n" );
    fclose( $handle );
}

// Usata per rendere gli indirizzi di posta illeggibili agli spambot.
function antispam($mail) {
    return "<span class=\"backwards\">".strrev( $mail )."</span>";
}

// Disegna una barra per le statistiche.
function statBar( $larghezza, $ok, $fuzzy, $ko ) {

$totale = $ok + $fuzzy + $ko ;

if ( $totale > 0 ) {
    // Calcolo percentuali
    $koP    = ceil( $ko * $larghezza / $totale );
    $fuzzyP = ceil( $fuzzy * $larghezza / $totale ) ;
    // Assicurati che la somma sia $larghezza
    $okP    = $larghezza - $fuzzyP - $koP;

    $messaggio = "$ok fatti, $fuzzy da verificare, $ko da fare";

    if ( $okP > 0 )
        echo "<img src=\"images/fatto.png\" style=\"width: ${okP}px; ".
             "height: 15px;\" alt=\"$ok fatti\" title=\"$messaggio\" />";
    if ( $fuzzyP > 0 )
        echo "<img src=\"images/fuzzy.png\" style=\"width: ${fuzzyP}px; ".
             "height: 15px;\" alt=\"$fuzzy da verificare\" ".
             "title=\"$messaggio\" />";
    if ( $koP > 0 )
        echo "<img src=\"images/dafare.png\" style=\"width: ${koP}px; ".
             "height: 15px;\" alt=\"$ko da fare\" title=\"$messaggio\" />";
} else        // Se è un messaggio vuoto
    echo "<img src=\"images/vuoto.png\" style=\"width: ${larghezza}px; ".
         "height: 15px;\" alt=\"Nessun messaggio nel file\" ".
         "title=\"Nessun messaggio nel file\" />";
}

// Disegna una torta per le statistiche
// Il raggio è funzione logaritmica del numero di messaggi
function statPie( $ok, $fuzzy, $ko ) {
    $size = ceil( log10( $ok+$fuzzy+$ko+1 )*10 );
    $message = "$ok fatti, $fuzzy da verificare, $ko da fare";
    echo "<img src='images/torta.php?".
         "t=$ok&amp;f=$fuzzy&amp;u=$ko&amp;size=$size' ".
         "title='$message' width='$size' height='$size'/>";
}

// Produce un'icona con come titolo l'autore, la data
// e l'ora dell'ultima modifica del file con Lokalize
function ultimeModifiche( $quando, $dachi ) {
    $msg = "Ultima modifica del $quando di $dachi.";
    echo "<img src=\"images/registro.png\" alt=\"$msg\" title=\"$msg\"/>";
}

// Recupera l'identificativo dell'utente responsabile di un file
function findOwner( $type, $package, $file, $isReview, $db ) {
    $idType= $isReview ? 'idReviewer' : 'idTranslator';
    $query = "SELECT $idType FROM files ".
             "WHERE file= ? AND package= ? AND type= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('sss', $file, $package, $type);
    $stmt->execute();
    $stmt->bind_result($ID);
    $stmt->fetch();
    $stmt->close();
    return $ID;
}

// Accetta o rifiuta la richiesta indicata di una traduzione o di una revisione
function handleRequest( $id, $accept, $db ) {
    $query = "SELECT review FROM requests WHERE idRequest= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($isReview);
    $stmt->fetch();
    $stmt->close();

    $role  = $isReview ? 'Reviewer' : 'Translator';
    $toSet = $accept ? "files.id${role}=requests.idFrom, decision='accept'" :
                       "decision='reject'";
    $query = "UPDATE files JOIN requests USING(file,package,type) ".
             "SET $toSet WHERE idRequest= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
}

function getName( $id, $db ) {
    $query = "SELECT CONCAT_WS(' ', first, last) FROM translators ".
             "WHERE idTranslator= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $stmt->bind_result($name);
    $stmt->fetch();
    $stmt->close();
    return $name;
}

// Produce una cella con nome di un traduttore o di un revisore
function nameCell( $name, $id, $role, $since ) {
    $da = $since ? "$role dal ".$since : "$role da sempre";
    echo "<td>";
    if($id)
        echo "<a href=\"traduttori.php#id-$id\" title=\"$da\">$name</a>";
    else
        echo "Non assegnato";
    echo "</td>\n";
}

function checkboxCell( $fileid, $fieldname ) {
    echo "<td style='text-align: center;'>";
    echo "<input type=\"checkbox\" value=\"$fileid\" name=\"${fieldname}[]\"/>";
    echo "</td>\n" ;
}


function statTable( $db, $WHERE='', $ORDER='file ASC',
                    $options=array(), $criteria=array() ) {

    //Controllo esistenza chiavi in array $options
    $nopackage    = in_array( "nopackage",    $options );
    $notype       = in_array( "notype",       $options );
    $notranslator = in_array( "notranslator", $options );
    $noreviewer   = in_array( "noreviewer",   $options );
    $nostatgraph  = in_array( "nostatgraph",  $options );

    deadlines( $db );

    echo "<h2>Statistiche dei file</h2>\n";
    if( !empty($criteria) ) {
        echo "<p>Criteri di selezione:</p>\n";
        echo "<ul>\n";
        foreach( $criteria as $criterion ) {
            echo '<li>';
            echo $criterion;
            echo '</li>';
        }
        echo "</ul>\n";
    }

    $query = "SELECT file, package, type, ".
            "CONCAT_WS(' ', translators.first, translators.last) AS tr_name, ".
            "translators.email AS tr_email, ".
            "translators.idTranslator AS tr_id, ".
            "CONCAT_WS(' ', reviewers.first, reviewers.last) AS rv_name, ".
            "reviewers.email AS rv_email, idReviewer AS rv_id, ".
            "done, fuzzy, todo, done+fuzzy+todo AS total, ".
            "DATE_FORMAT(translatorSince, '%d/%m/%Y, %H:%i') AS tr_since, ".
            "DATE_FORMAT(reviewerSince, '%d/%m/%Y, %H:%i') AS rv_since, ".
            "lastModified, lastModifiedBy ".
            "FROM files NATURAL JOIN stats NATURAL LEFT JOIN translators ".
            "LEFT JOIN translators AS reviewers ".
                    "ON idReviewer=reviewers.idTranslator ";
    if ($WHERE)
        $query .= "WHERE $WHERE ";
    if ($ORDER)
        $query .= "ORDER BY $ORDER";

    $result = $db->query($query);
    if( $db->error ) {
        echo "<p>Si è verificato un errore:<br/><tt>".$db->error."</tt></p>";
        return;
    }

    if ( $result->num_rows == 0 ) {
    ?>
<fieldset class="it_feedback">
<legend>Nessun risultato</legend>
Questo elenco non contiene nessun file.
</fieldset>
    <?php
        return;
    }

    echo "<div style=\"width: 80%; margin:auto;\">\n";
    $dataora = lastStats( $db );
    echo "<p style='float: left'>Ultime statistiche: ".
         "<strong>$dataora</strong></p>";

    // Crea link per andare a statistiche corrispondenti per GUI, DOC e tutto
    echo "\n<p style='float: right'>\n";
    $newGET = $_GET;
    $newGET['type']='GUI';
    $link = htmlentities( $_SERVER['PHP_SELF'].'?'.http_build_query($newGET) );
    echo "<a href='$link'>[GUI]</a>&nbsp;\n";
    $newGET['type']='DOC';
    $link = htmlentities( $_SERVER['PHP_SELF'].'?'.http_build_query($newGET) );
    echo "<a href=\"$link\">[DOC]</a>&nbsp;\n";
    unset( $newGET['type'] );
    $link = htmlentities( $_SERVER['PHP_SELF'].'?'.http_build_query($newGET) );
    echo "<a href='$link'>[Tutto]</a>\n";
    echo "</p>\n";
    echo "</div>\n";

    echo "<form action=\"richieste.php\" method=\"post\">\n";
    echo '<table class="it_stat" style="clear: both" data-sortable="data-sortable">'."\n";

    // Intestazioni della tabella
    echo "<thead><tr><th>File</th>";
    if(!$nopackage)
        echo "<th>Pacchetto</th>\n";
    if(!$notype) // GUI o DOC
        echo "<th>Tipo</th>\n";
    if(!$notranslator)
        echo "<th>Traduttore</th>\n";
    if(!$noreviewer)
        echo "<th>Revisore</th>\n";
    echo "<th>Totale</th>\n";
    if(!$nostatgraph)
        echo "<th data-sortable=\"false\"></th>\n";
    echo "<th style=\"text-align: center;\">Trad</th>\n";
    echo "<th style=\"text-align: center;\">Rev</th>\n";
    echo "</tr></thead>\n";

    echo "<tbody>\n";
    // Righe della tabella
    while ( $row = $result->fetch_assoc() ) {

        if( $row["done"]==$row["total"] )
            $bgstyle="ok";
        elseif( $row["todo"]==$row["total"] )
            $bgstyle="ko";
        else
            $bgstyle="fuzzy";

        // Costruisci percorso al file su WebSVN
        $filepath  = ( $row["type"]=='DOC' ) ? "docmessages/" : "messages/";
        $filepath .= $row["package"]."/".$row["file"];
        $filepath .= ( $row["done"] + $row["fuzzy"] > 0 ) ? ".po" : ".pot";
        $lang      = ( $row["done"] + $row["fuzzy"] > 0 ) ? "it" : "templates";
        $folder    = "trunk/l10n-support/$lang/summit";
        $svnlink   = "https://websvn.kde.org/$folder/$filepath?view=co";
        $repolink  = "https://websvn.kde.org/$folder/$filepath";

        // Questi sono i contenuti delle colonne della tabella. Cfr. sopra.
        echo "\n<tr class=\"$bgstyle\">\n";

        echo "<td>";
        if ( $row["lastModified"] && $row["lastModifiedBy"] )
            ultimeModifiche( $row["lastModified"], $row["lastModifiedBy"] );
        echo "<a href=\"$repolink\"><img src=\"images/subversion.png\" ".
            "title=\"Cronologia di Subversion\" alt=\"Cronologia\"/></a>";
        echo "<a href=\"$svnlink\" type=\"text/plain\">".$row["file"]."</a>";
        echo "</td>\n";

        if(!$nopackage) {
            echo "<td>";
            echo "<a href=\"file.php?package=".$row["package"]."\">";
            echo $row["package"];
            echo "</a>";
            echo "</td>\n";
        }

        if(!$notype) {
            echo "<td>";
            echo "<a href=\"file.php?package=".$row["package"].
                "&amp;type=".$row["type"]."\">";
            echo $row["type"];
            echo "</a>";
            echo "</td>\n";
        }

        if(!$notranslator)
            nameCell($row["tr_name"],$row["tr_id"],$row["tr_since"],
                     "Traduttore");

        if( !$noreviewer)
            nameCell($row["rv_name"],$row["rv_id"],$row["rv_since"],"Revisore");

        $messaggio = $row["done"]." fatti, ".$row["fuzzy"]." da verificare, ".
                    $row["todo"]." da fare";
        echo "<td style='text-align: right;'>";
        echo "<span title=\"$messaggio\">".$row["total"]."</span>";
        echo "</td>\n";

        if(!$nostatgraph) {
            echo "<td style='text-align: center;'>";
            statPie($row["done"], $row["fuzzy"], $row["todo"]);
            echo "</td>\n";
        }

        $fileid = $row["type"]."/".$row["package"]."/".$row["file"];
        checkboxCell( $fileid, "translations" );
        checkboxCell( $fileid, "reviews" );

        echo "</tr>\n";
    }
    echo "</tbody>\n";
    $result->free();

    // Ultima riga di riassunto
    $query = "SELECT SUM(done) AS done, SUM(fuzzy) AS fuzzy, ".
             "SUM(todo) AS todo, SUM(done+fuzzy+todo) AS total ".
             "FROM files NATURAL JOIN stats ";
    if ($WHERE)
        $query .= "WHERE $WHERE ";

    $totals = $db->query($query)->fetch_assoc();
    $firstcolspan = 5;
    $lastcolspan  = 2;

    if($nopackage)
        $firstcolspan -= 1;
    if($notype)
        $firstcolspan -= 1;
    if($notranslator)
        $firstcolspan -= 1;
    if($noreviewer)
        $firstcolspan -= 1;

    echo "<tfoot><tr>\n";
    echo "<th colspan=\"$firstcolspan\">Totale</th>\n";

    $messaggio = $totals["done"]." fatti, ".$totals["fuzzy"]." da verificare, ".
                $totals["todo"]." da fare";
    echo "<th><span title=\"$messaggio\">".$totals["total"]."</span></th>\n";

    if(!$nostatgraph) {
        echo "<th>";
        statPie( $totals["done"], $totals["fuzzy"], $totals["todo"] );
        echo "</th>\n";
    }

    echo "</tr></tfoot>\n";

?>
</table>
<script type="text/javascript" src="sortable.min.js"></script>

<div style="text-align: center;">
<p style="margin-top: 2em;">Scrivi sotto dei commenti se devi dire
qualcosa ai proprietari dei file che stai richiedendo.</p>
<textarea name="comment" cols="50" rows="6"></textarea><br/>

<button type="submit">Richiedi</button>
</div>

</form>

<fieldset class="it_feedback"><legend>Legenda</legend>
<table>
<tr>
<td></td>
<td>Tieni il mouse sulle statistiche per avere il conto dettagliato dei
messaggi.</td>
</tr>
<tr>
<td><img src="images/subversion.png" height="20px" alt="Deposito Subversion"
         title="Deposito Subversion" /></td>
<td>Collegamento alla cronologia di <a href="https://websvn.kde.org/">WebSVN</a>,
utile per recuperare versioni precedenti del file con il tuo browser.</td>
</tr>
<tr>
<td><img src="images/registro.png" height="20px" alt="Ultima modifica"
         title="Ultima modifica" /></td>
<td>Quando e da chi è stata apportata l'ultima modifica sul file
(con Lokalize).</td>
</tr>
</table>
<p><strong>Attenzione:</strong> la banca dati viene aggiornata
<em>periodicamente</em>: i cambiamenti nel deposito di Subversion <em>non</em>
si riflettono immediatamente qui.</p>
</fieldset>

<?php

}

/**
 * Stampa la data di ultima modifica della tabella stats.
 * NOTE Il nuovo motore InnoDB, già predefinito in MySQL, non supporta
 *      l'aggiornamento di UPDATE_TIME e restituirà NULL.
 *      Assicurarsi che almeno stats usi il motore MyISAM.
 */
function lastStats( $db ) {
    $query = "SELECT DATE_FORMAT(UPDATE_TIME, '%d/%m/%Y, %H:%i') AS last ".
             "FROM information_schema.tables ".
             "WHERE TABLE_SCHEMA='i18n_it' AND TABLE_NAME='stats'";
    $result = $db->query($query);
    $last = $result->fetch_object()->last;
    $result->free();
    return $last;
}

// Invia messaggi di posta elettronica con intestazioni corrette del sito
function inviaPosta( $a, $oggetto, $messaggio, $rispondi_a='' ) {
    $intestazioni = "From: Gruppo italiano KDE-i18n <kde-i18n-it@kde.org>\r\n".
                    "Content-Type: text/plain; charset=\"utf-8\"\r\n";
    if ( !empty( $rispondi_a ) )
        $intestazioni .= "Reply-To: $rispondi_a\r\n";
    return mail( $a, $oggetto, $messaggio, $intestazioni );
}

// Genera un indice di pagine data la pagina attuale, il numero totale di
// elementi, il numero di elementi per pagina ed eventualmente il numero di
// pagine vicine da collegare nell'indice
function pageIndex ($page, $number, $perpage, $span=9) {
    $pages = ceil( $number/$perpage );
    if( $pages < 2 ) // Niente da fare
        return;

    $newGET = $_GET;
    unset( $newGET['page'] );
    $selflink = htmlentities( $_SERVER['PHP_SELF'].'?'.
                              http_build_query($newGET) );

    echo "<p style='text-align:center;font-size:larger'>";

    $first = $page - floor(($span-1)/2);
    $last  = $page + ceil(($span-1)/2);
    // Se uno dei limiti è oltre il dominio, vanno entrambi spostati
    if( $first < 1 ) {
        $first = 1;
        $last  = min($span, $pages);
    } elseif( $last > $pages ) {
        $first = max($pages-$span, 1);
        $last  = $pages;
    }

    // Collegamento all'inizio
    echo "<a href='$selflink&amp;page=1'>«</a>&nbsp;\n";
    // Collegamento a $span pagine indietro
    echo "<a href='$selflink&amp;page=".max($page-$span,1)."'>‹</a>&nbsp;\n";
    // Collegamento a pagine dell'indice
    for( $i = $first; $i <= $last; ++$i ) {
        echo ($i==$page) ? "<strong>$i</strong>&nbsp;\n" :
                           "<a href='$selflink&amp;page=$i'>$i</a>&nbsp;\n";
    }
    // Collegamento a $span pagine avanti
    echo "<a href='$selflink&amp;page=".min($page+$span,$pages)."'>›</a>".
         "&nbsp;\n";
    // Collegamento alla fine
    echo "<a href='$selflink&amp;page=$pages'>»</a>&nbsp;\n";
    echo "<br/>";
    echo "Risultati ".(($page-1)*$perpage+1)."&ndash;".
         min($page*$perpage, $number)." di $number";
    echo "</p>\n";
}

?>
