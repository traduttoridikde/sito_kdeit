<?php
/**
*
* File di rilevazione file con nomi doppi nel deposito.
*
*/

define('ASSOLI_SCRIPT', 1);

include("header.php");
require_once( 'configurazione.php' );
require_once( 'connessione.php' );

?>

<h1>Ricerca dei file presenti in più di una cartella</h1>
<p>I file qui elencati sono presenti con lo stesso nome in diverse cartelle
del deposito di Subversion. Potrebbe essere un'omonimia, o un file tenuto
apposta in due cartelle a causa di diversi piani di rilascio.</p>

<?php

$query = "SELECT file, type, COUNT(*) AS number FROM files ".
         "GROUP BY file, type HAVING number > 1 ORDER BY number DESC, file";
$result = $db->query($query);

if ( $result->num_rows == 0 ) {
?>
<fieldset class="it_feedback">
<legend>Nessun risultato</legend>
Non risulta alcun doppione in tutto il deposito. Va tutto bene.
</fieldset>
<?php
}

echo "<ul>\n";
$query = "SELECT package FROM files WHERE file= ? AND type= ?";
$stmt  = $db->prepare($query);
$stmt->bind_param('ss', $file, $type);
$stmt->bind_result($package);

while ( $row = $result->fetch_assoc() ) {
    $file = $row['file'];
    $type = $row['type'];
    echo "<li>$file ($type), trovato in: ";
    $packages = array();
    $stmt->execute();
    while ( $stmt->fetch() )
        $packages[] = $package;
    echo implode( ", ", $packages );
    echo "</li>\n";
}
echo "</ul>\n";

include("footer.php");

?>