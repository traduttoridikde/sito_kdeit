<?php
header('Content-type: image/svg+xml');
header('Content-encoding: x-gzip');

$translated = $_GET["t"];
$fuzzy = $_GET["f"];
$untranslated = $_GET["u"];
$size = $_GET["size"];

$total = $translated + $fuzzy + $untranslated;
if( $total == 0 )
    return;

$gradientcx = $size / 4 * 3;
$gradientcy = $size / 4;
$radius = $size / 2;

// The angles swept by the Translated and Fuzzy slices
$angleT = $translated * 2 * pi() / $total ;
$angleF = $fuzzy * 2 * pi() / $total ;

// The coordinates of the circumferential junction points of the slices
$firstX = cos( $angleT ) * $radius + $radius;
$firstY = -sin( $angleT ) * $radius + $radius;
$secondX = cos( $angleT+$angleF ) * $radius + $radius;
$secondY = -sin( $angleT+$angleF ) * $radius + $radius;

// Large arc flag: set to 1 if the angle is larger than 180°
$largeArcT = 0;
$largeArcF = 0;
$largeArcU = 0;

if( $angleT > pi() )
    $largeArcT = 1;
if( $angleF > pi() )
    $largeArcF = 1;
if( $angleT + $angleF < pi() )
    $largeArcU = 1;

$singleComponent = ( $total == $translated   ||
                     $total == $fuzzy        ||
                     $total == $untranslated );

$svg .="<?xml version=\"1.0\" standalone=\"no\"?>\n";
$svg .="<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
$svg .="<svg width=\"$size\" height=\"$size\" viewBox=\"0 0 $size $size\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\n";
$svg .="<title>Statistiche di un file PO</title>\n";
$svg .="\t<defs>\n";
$svg .="\t\t<radialGradient id=\"verde\" gradientUnits=\"userSpaceOnUse\" cx=\"$gradientcx\" cy=\"$gradientcy\" r=\"$size\">\n";
$svg .="\t\t\t<stop offset=\"0%\" stop-color=\"green\" stop-opacity=\"30%\"/>\n";
$svg .="\t\t\t<stop offset=\"30%\" stop-color=\"green\" stop-opacity=\"50%\"/>\n";
$svg .="\t\t\t<stop offset=\"100%\" stop-color=\"green\" stop-opacity=\"100%\"/>\n";
$svg .="\t\t</radialGradient>\n";
$svg .="\t\t<radialGradient id=\"blu\" gradientUnits=\"userSpaceOnUse\" cx=\"$gradientcx\" cy=\"$gradientcy\" r=\"$size\">\n";
$svg .="\t\t\t<stop offset=\"0%\" stop-color=\"blue\" stop-opacity=\"30%\"/>\n";
$svg .="\t\t\t<stop offset=\"30%\" stop-color=\"blue\" stop-opacity=\"50%\"/>\n";
$svg .="\t\t\t<stop offset=\"100%\" stop-color=\"blue\" stop-opacity=\"100%\"/>\n";
$svg .="\t\t</radialGradient>\n";
$svg .="\t\t<radialGradient id=\"rosso\" gradientUnits=\"userSpaceOnUse\" cx=\"$gradientcx\" cy=\"$gradientcy\" r=\"$size\">\n";
$svg .="\t\t\t<stop offset=\"0%\" stop-color=\"red\" stop-opacity=\"30%\"/>\n";
$svg .="\t\t\t<stop offset=\"30%\" stop-color=\"red\" stop-opacity=\"50%\"/>\n";
$svg .="\t\t\t<stop offset=\"100%\" stop-color=\"red\" stop-opacity=\"100%\"/>\n";
$svg .="\t\t</radialGradient>\n";
$svg .="\t</defs>\n\n";

if( $singleComponent ) {
    if ( $translated != 0)
        $color = "verde";
    else if ( $fuzzy != 0 )
        $color = "blu";
    else if ( $untranslated != 0 )
        $color = "rosso";
    $svg .="\t<circle cx=\"$radius\" cy=\"$radius\" r=\"$radius\" fill=\"url(#$color)\"/>\n";
} else {
    $svg .="\t<path id=\"translated\" d=\"M $radius, $radius L $size, $radius A $radius, $radius, 0, $largeArcT, 0, $firstX, $firstY\" fill=\"url(#verde)\"/>\n";
    $svg .="\t<path id=\"fuzzy\" d=\"M $radius, $radius L $firstX, $firstY A $radius, $radius, 0, $largeArcF, 0, $secondX, $secondY\" fill=\"url(#blu)\"/>\n";
    $svg .="\t<path id=\"untranslated\" d=\"M $radius, $radius L $secondX, $secondY A $radius, $radius, 0, $largeArcU, 0, $size, $radius\" fill=\"url(#rosso)\"/>\n";
}
$svg .= "</svg>\n";

echo gzencode($svg);
?>