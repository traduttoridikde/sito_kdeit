<?php
/**
 * Modulo di modifica dati e registrazione utenti
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
require_once( 'func_reg.php' );


$biography     = chkvar( $_POST['biography'] );
$email         = chkvar( $_POST['email'] );
$file          = chkvar( $_FILES['file'] );
$first         = chkvar( $_POST['first'] );
$id            = chkvar( $_POST['id'] );
$last          = chkvar( $_POST['last'] );
$modify        = chkvar( $_GET['modify'], FALSE );
$oldpassword   = chkvar( $_POST['oldpassword'] );
$newpassword   = chkvar( $_POST['password'], NULL );
$passwordCheck = chkvar( $_POST['passwordCheck'] );
$submit        = chkvar( $_POST['submit'], FALSE );

$biography = htmlspecialchars( $biography );

if ( $submit )
    $modify = true;

if ( $modify )
    require_once( 'identifica.php' );

$query = "SELECT idTranslator FROM translators WHERE email= ?";
$stmt  = $db->prepare($query);
$stmt->bind_param('s', $_SERVER['PHP_AUTH_USER']);
$stmt->execute();
$stmt->bind_result($USERID);
$stmt->fetch();
$stmt->close();

include( 'header.php' );

if ($submit) {
    if( $newpassword!=$passwordCheck ) {
        echo "<h1>Errore</h1>\n";
        echo '<p class="it_feedback">Sono state fornite due password '.
             "diverse.</p>\n";
    } elseif ( !$first || !$last || !$email) {
        echo "<h1>Errore</h1>\n";
        echo '<p class="it_feedback">Non sono stati forniti tutti i campi '.
             "obbligatori.</p>\n";
    } elseif( $id != $USERID ) {
        echo "<h1>Errore</h1>\n";
        echo '<p class="it_feedback">Stai cercando di modificare i dati di un '.
             "altro traduttore. Che scherzi sono?</p>\n";
    } elseif( $id && !checkUserPassword( $id, $oldpassword, $db ) ) {
        echo "<h1>Errore</h1>\n";
        echo '<p class="it_feedback">La password corrente fornita non '.
             "corrisponde a quella nella banca dati.</p>\n";
    } else {
        $newpassword = $newpassword ? $newpassword : $oldpassword;
        editUser($first, $last, $email, $newpassword, $file, $biography, $db, $id);
    }

}

?>

<div class="it_trad">

<?php
echo "<h2 style=\"color: white;\">\n";
if ( $modify ) {
    $query = "SELECT first, last, email, idTranslator, biography ".
             "FROM translators WHERE email= ?";
    $stmt  = $db->prepare($query);
    $stmt->bind_param('s', $_SERVER['PHP_AUTH_USER']);
    $stmt->execute();
    // NOTE mysqli_stmt::store_result è assolutamente necessario qui perché
    //      biography è un campo longtext di lunghezza non definita, e senza
    //      store_result PHP esaurisce immediatamente la memoria perché cerca
    //      di allocarne infinita.
    $stmt->store_result();
    $stmt->bind_result($first, $last, $email, $id, $biography);
    $stmt->fetch();
    $stmt->close();

    echo "<img src=\"foto.php?id=$id\" ".
         'alt="Immagine attuale" title="Questa è la tua immagine attuale" '.
         'class="framed"/>Modifica i tuoi dati';
} else
    echo "Diventa traduttore di KDE";
echo "</h2>";
?>

<form method="post" enctype="multipart/form-data" action="register.php">
<table style="border: 0px;">
<tr>
<td><strong>Nome</strong></td>
<td><input type="text" name="first" <?php
           if ( $modify ) echo "value=\"$first\"";
           ?> /></td>
</tr>

<tr>
<td><strong>Cognome</strong></td>
<td><input type="text" name="last" <?php
           if ( $modify ) echo "value=\"$last\"";
           ?> /></td>
</tr>

<tr>
<td><strong>Posta elettronica</strong></td>
<td><input type="text" name="email" <?php
           if ( $modify ) echo "value=\"$email\"";
           ?> /></td>
</tr>

<tr>
<td><?php echo $modify ? 'Nuova password' : 'Password'; ?></td>
<td><input type="password" name="password"/></td>
</tr>

<tr>
<td>Ripeti password</td>
<td><input type="password" name="passwordCheck"/></td>
</tr>

<tr>
<td>Foto o avatar</td>
<td><input type="file" name="file"/></td>
</tr>

<tr>
<td>Biografia</td>
<td><textarea name="biography" cols="60" rows="6"><?php
              if ( $modify ) echo $biography;
              ?></textarea></td>
</tr>
<?php
if ( $modify ) {
    echo "<input type=\"hidden\" name=\"id\" value=\"$USERID\"/>\n";
    echo '<tr><td><strong>Vecchia password</strong></td>'.
         '<td><input type="password" name="oldpassword" /></td></tr>';
}
?>

</table>

<p style="text-align: center">
<button type="submit" name="submit" value="true">Inserisci valori</button>
</p>
</form>

<p>I campi in grassetto sono obbligatori.</p>

</div>

<?php

include("footer.php");

?>
