<?php
/**
 * Approvazione o rifiuto delle richieste dei file
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
include('functions.php');
include('checkadmin.php');

$request  = chkvar( $_POST["request"] );
$decision = chkvar( $_POST["decision"] );

include_once("header.php");

if( $decision ) {
    $accept = ($decision == 'accept');
    handleRequest( $request, $accept, $db );
    if( $accept ) { // Se accettato, rimuovi richieste concorrenti
        $query = 'SELECT idRequest FROM requests NATURAL JOIN '.
                 '(SELECT file, type, package, review FROM requests '.
                  'WHERE idRequest= ? ) AS accepted '.
                 'WHERE decision IS NULL';
        $stmt  = $db->prepare($query);
        $stmt->bind_param('i', $request);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($idRequest);
        while( $stmt->fetch() )
            handleRequest( $idRequest, FALSE, $db );
        $stmt->close();
    }
}

?>

<h1>Richieste di assegnazione in corso</h1>
<table class="it_pack it_centrata" style="margin-top: 2em">
<tr>
<th>Data</th>
<th>Richiesta</th>
<th>Azione</th>
</tr>

<?php
$query = 'SELECT idRequest, since, type, package, file, review, concurrent, '.
         "CONCAT_WS(' ', owners.first, owners.last) AS owner, ".
         "CONCAT_WS(' ', requesters.first, requesters.last) AS requester ".
         'FROM requests NATURAL JOIN '.
         '(SELECT COUNT(*)-1 AS concurrent, type, file, package, review '.
          'FROM requests WHERE decision IS NULL '.
          'GROUP BY type, file, package, review) AS concurrent '.
         'NATURAL LEFT JOIN translators AS owners '.
         'LEFT JOIN translators AS requesters '.
                   'ON idFrom=requesters.idTranslator '.
         'WHERE decision IS NULL ORDER BY since, idRequest';
$pending = $db->query($query);

while ( $r = $pending->fetch_assoc() ) {
    echo '<tr>';
    echo '<td style="white-space: nowrap;">'.
         strftime("%e %B %Y", strtotime($r['since']))."</td>\n";
    echo '<td>'.$r['requester'].' chiede a '.$r['owner'].' di ';
    echo $r['review'] ? 'rileggere' : 'tradurre';
    echo ' il file '.($r['type']=='GUI' ? "dell'interfaccia" :
                                          'della documentazione');
    echo ' '.$r['package'].'/'.$r['file'];
    echo "</td>\n";
?>
<td>
<form action='<?php echo $_SERVER['PHP_SELF'] ?>' method='post'>
<input type='hidden' name='request' value='<?php echo $r['idRequest'] ?>'/>
<button type='submit' name='decision' value='accept'
        onclick='return confirm("Approvi la richiesta?
<?php
    if( $r['concurrent'] > 0 )
        echo 'Attenzione: '.$r['concurrent'].' richieste concorrenti '.
             'per lo stesso file saranno rifiutate.';
?>")'>
<img src='images/ok.png' alt='Accetta' title='Accetta'/>
</button>
<button type='submit' name='decision' value='refuse'
        onclick='return confirm("Rifiuti la richiesta?")'>
<img src='images/ko.png' alt='Rifiuta' title='Rifiuta'/>
</button></form>
</td>
</tr>

<?php
}
echo "</table>";


include("footer.php");
?>
