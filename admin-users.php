<?php
/**
 * Amministrazione degli account degli utenti
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
include_once( 'checkadmin.php') ;
include_once( 'header.php' );
include_once( 'functions.php' );

if ( $_POST["idMod"] ) {

    $deprive = chkvar( $_POST["deprive"] );
    $remove  = chkvar( $_POST["remove"] );
    $modify  = chkvar( $_POST["modify"] );

    if( $deprive or $remove ) {
        $queries = array();
        // Togli traduzioni, revisioni e pacchetti
        $queries[] = 'UPDATE files SET idTranslator=NULL WHERE idTranslator= ?';
        $queries[] = 'UPDATE files SET idReviewer=NULL WHERE idReviewer= ?';
        $queries[] = 'UPDATE packages SET idTranslator=NULL '.
                     'WHERE idTranslator= ?';
        if( $remove ) // Rimuovi traduttore
            $queries[] = "DELETE FROM translators WHERE idTranslator= ?";

        foreach( $queries as $query ) {
            $stmt  = $db->prepare($query);
            $stmt->bind_param('i', $_POST["idMod"]);
            $stmt->execute();
            $stmt->close();
        }

    } elseif( $modify ) {
        $first     = chkvar( $_POST["first"] );
        $last      = chkvar( $_POST["last"] );
        $email     = chkvar( $_POST["email"] );
        $password  = chkvar( $_POST["password"] );
        $biography = chkvar( $_POST["biography"] );
        $admin     = chkvar( $_POST["admin"], '0' );

        $query = 'UPDATE translators SET first= ?, last= ?, email= ?, '.
                 'biography= ?, admin= ? WHERE idTranslator= ?';
        $stmt  = $db->prepare($query);
        $stmt->bind_param( 'sssssi', $first, $last,
                           $email, $biography,
                           $admin, $_POST["idMod"] );
        $stmt->execute();
        $stmt->close();

        if( !empty($password) ) {
            $query = 'UPDATE translators SET password= ? WHERE idTranslator= ?';
            $stmt  = $db->prepare($query);
            $stmt->bind_param('si', crypt($password), $_POST["idMod"]);
            $stmt->execute();
            $stmt->close();
        }

    } else { //Nessuna azione sull'utente indicato: proponi cosa fare

        $query = 'SELECT first, last, email, biography, admin '.
                 'FROM translators WHERE idTranslator= ?';
        $stmt  = $db->prepare($query);
        $stmt->bind_param('i', $_POST["idMod"]);
        $stmt->execute();
        // NOTE usa store result perché stai estraendo $biography che è longtext
        $stmt->store_result();
        $stmt->bind_result($first, $last, $email, $biography, $admin);
        $stmt->fetch();
        $stmt->close();

?>
        <h2>Gestione dell'utente <?php echo $first." ".$last ?>:</h2>
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <input type="hidden" name="idMod" value="<?php
        echo $_POST["idMod"]; ?>"/>
        <ul>
        <li>Nome:
        <input type="text" name="first" value="<?php echo $first; ?>"/></li>
        <li>Cognome:
        <input type="text" name="last" value="<?php echo $last; ?>"/></li>
        <li>Nuova password:
        <input type="text" name="password"/></li>
        <li>Posta:
        <input type="text" name="email" value="<?php echo $email; ?>"/></li>
        <li>Amministratore:
        <input type="checkbox" name="admin" value="1" <?php
        echo $admin ? 'checked="checked"' : ''; ?> /></li>
        </ul>
        <fieldset><legend>Biografia</legend>
        <textarea name="biography" rows="6" cols="60"><?php
        echo $biography; ?></textarea>
        </fieldset>
        <p><button name="modify" value="true">Invia modifiche</button><br/>
        <button name="deprive" value="true" onclick="return confirm('Sei sicuro
        di voler togliere tutti i file e pacchetti a
        <?php echo $first." ".$last ?>?')" >Togli tutti i file</button><br/>
        <button name="remove" value="true" onclick="return confirm('Sei sicuro
        di voler eliminare l\x27account di <?php echo $first." ".$last ?>?')">
        Elimina account</button></p>
        </form>
<?php
    }
}

?>
<h1>Traduttori attualmente registrati</h1>
<p>Amministratori indicati in grassetto.</p>
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
<table class="it_stat">
<tr>
<th rowspan="2">Nome e cognome</th>
<th rowspan="2">ID</th>
<th rowspan="2">Posta</th>
<th rowspan="2">File</th>
<th colspan="2">Richieste</th>
<th rowspan="2">Foto</th>
</tr>
<tr>
<th>A</th>
<th>Da</th>
</tr>
<?php

$query = 'SELECT idTranslator, email, NOT ISNULL(foto) AS hasPic, admin, '.
                "CONCAT_WS(' ', first, last) AS name, ".
                'COALESCE(filecounts.files, 0) AS filecount, '.
                'COALESCE(received.received, 0) AS received, '.
                'COALESCE(sent.sent, 0) AS sent '.
         'FROM translators NATURAL LEFT JOIN '.
              '(SELECT idTranslator, COUNT(*) AS files FROM files '.
              'GROUP BY idTranslator) AS filecounts '.
         'NATURAL LEFT JOIN '.
              '(SELECT idTranslator, COUNT(*) AS received FROM requests '.
              'GROUP BY idTranslator) AS received '.
         'NATURAL LEFT JOIN '.
              '(SELECT idFrom AS idTranslator, COUNT(*) AS sent FROM requests '.
              'GROUP BY idFrom) AS sent '.
         'GROUP BY idTranslator ORDER BY first, last, email';
$result = $db->query($query);

while ( $r = $result->fetch_assoc() ) {
    echo $r['filecount'] > 0 ? "\n<tr>\n" : "\n<tr class='ko'>\n";
    echo $r['admin'] ? '<td style="font-weight: bold;">' : '<td>';
    echo $r['name']."</td>\n";
    echo '<td>'.$r['idTranslator']."</td>\n";
    echo '<td>'.antispam($r['email'])."</td>\n";
    echo '<td>'.$r['filecount']."</td>\n";
    echo '<td>'.$r['received']."</td>\n";
    echo '<td>'.$r['sent']."</td>\n";
    echo $r['hasPic'] ? "<td>Sì</td>\n" : "<td>No</td>\n";
    echo '<td><button name="idMod" value="'.$r['idTranslator'].'">';
    echo "Modifica</button></td>\n";
    echo "</tr>\n";
}
echo "</table></form>\n";


include("footer.php");
?>
