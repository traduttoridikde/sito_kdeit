<?php
/**
 * File di configurazione generale.
 */

// Configurazione della banca dati
require("wallet.php");
$DB_host   = 'localhost';
$DB_nomedb = 'i18n_it';

// Configurazione del sito

setlocale( LC_TIME, "it_IT.UTF8" );
date_default_timezone_set( "Europe/Rome" );

$activate_debug = false;

$calendarFile = 'kde-it.ics';

////////
//////// FINE CONFIGURAZIONE -- NON TOCCARE NULLA SOTTO QUESTA RIGA ////////
////////

// Controllo che impedisce il richiamo diretto del file include dall'esterno.
if( !defined('ASSOLI_SCRIPT') ) {
    header('HTTP/1.1 404 Not Found');
?>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL <?php $_SERVER['REQUEST_URI'] ?> was not found on this server.</p>
<hr>
<address><?php $_SERVER['SERVER_SIGNATURE'] ?> Server at kdeit.softwarelibero.it Port 80</address>
</body></html>
<?php
    exit;
}

// Controllo dell'esistenza delle variabili
function chkvar( & $var, $default=NULL ) {
    return isset($var) ? $var : $default;
}

// Funzione di visualizzazione degli errori PHP
function debug_errorHandler($errno, $errstr, $errfile, $errline) {
    print("<center><h1>Errore PHP</h1></center><br/><strong>Codice</strong>: ".
          "$errno<br/><strong>Descrizione</strong>: ".
          "$errstr<br/><strong>Riga</strong>: ".
          "$errline<br/><strong>File</strong>: ".
          "$errfile<br/><br/>");
}

if ( $activate_debug ) {
    error_reporting(E_ALL);
    set_error_handler('debug_errorHandler');
}


?>
