<?php

function usaPredefinita() {
    $nomeFile = "images/senzafoto.png";
    $handle   = fopen( $nomeFile, 'rb' );
    $dati     = fread( $handle, filesize($nomeFile) );
    fclose($handle);
    header("Content-type: image/png");
    echo $dati;
    exit();
}

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );

$id = chkvar($_GET["id"]);

if( !isset($id) )
    usaPredefinita();

$query = "SELECT foto, tipofoto FROM translators WHERE idTranslator= ?";
$stmt  = $db->prepare($query);
$stmt->bind_param('i', $id);
$stmt->bind_result($foto, $tipofoto);
$stmt->execute();

if( !$stmt->fetch() )
    usaPredefinita();

if( empty($foto) )
    usaPredefinita();

header("Content-type: $tipofoto");
echo $foto;

?>