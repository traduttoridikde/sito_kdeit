<?php
/**
 * Gestione delle scadenze
 */

define('ASSOLI_SCRIPT', 1);

require_once( 'configurazione.php' );
require_once( 'connessione.php' );
include('functions.php');
include('checkadmin.php') ;

include_once("header.php");

if ( $_POST["set"] )
{
    $date = $_POST['year'].'-'.$_POST['month'].'-'.$_POST['day'];
    if( checkDate($_POST['month'], $_POST['day'], $_POST['year']) ) {
        $query = "INSERT INTO deadlines (name, date) VALUES( ?, ? )";
        $stmt  = $db->prepare($query);
        $stmt->bind_param('ss', $_POST['name'], $date);
        $stmt->execute();
        echo "<p>Scadenza impostata.</p>\n";
        create_calendar( $db, $calendarFile );
    } else
        echo "<p><strong>Attenzione: data non valida</strong></p>\n";
}

if ( $_POST["delete"] )
{
    foreach( $_POST['deadlines'] as $deadline ) {
        $query = "DELETE FROM deadlines WHERE name= ?";
        $stmt  = $db->prepare($query);
        $stmt->bind_param('s', $deadline);
        $stmt->execute();
        echo "<p>Scadenza \"$deadline\" eliminata.</p>";
    }
    create_calendar( $db, $calendarFile ); 
}

?>

<h1>Scadenze</h1>
<p>Imposta qui una nuova scadenza. Inserisci una breve descrizione dei pacchetti
che la scadenza riguarda (per esempio <tt>KDE 4.0</tt>) e la data della
scadenza.</p>
<p>Verrà visualizzato un messaggio di avvertimento su tutte le pagine delle
statistiche da oggi fino al giorno della scadenza.</p>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<table class="it_pack it_centrata">
<tr>
<th>Descrizione<br/><small>Max 20 caratteri</small></th>
<th>Giorno</th>
<th>Mese</th>
<th>Anno</th>
</tr>

<tr>
<td>
<input type="text" size="20" name="name"/>
</td>

<td>
<select name="day">
<?php
foreach( range(1, 31) as $day ) {
    echo ($day == date('j')) ? '<option selected="selected">' : '<option>';
    echo "$day</option>\n";
}
?>
</select>
</td>

<td>
<select name="month">
<?php
foreach( range(1, 12) as $month ) {
    $monthName = ucfirst( strftime( "%B", strtotime("2000-$month-1") ) );
    echo "<option value=\"$month\"";
    echo ($month == strftime( "%m" )) ? ' selected="selected"' : '';
    echo ">$monthName</option>\n";
}
?>
</select>
</td>

<td>
<select name="year">
<option selected="selected"><?php echo date("Y"); ?></option>
<option><?php echo ( date("Y") + 1 ); ?></option>
</select>
</td>

</tr>
</table>

<div style="text-align: center;"><br/>
<button type="submit" name="set" value="TRUE">Imposta scadenza</button>
</div>
</form>

<h2>Rimozione delle scadenze</h2>
<p>Se vuoi rimuovere una scadenza, selezionala dall'elenco seguente.</p>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<table class="it_pack it_centrata">
<tr>
<th>Descrizione</th>
<th>Data</th>
<th style="text-align: center;">X</th>
</tr>

<?php
$query = 'SELECT name, date FROM deadlines ORDER BY date DESC';
$deadlines = $db->query($query);
while( $deadline = $deadlines->fetch_assoc() ) {
    $name = $deadline['name'];
    $date = ucfirst( strftime("%A %e %B %Y", strtotime($deadline['date'])) );
    echo "<tr>\n";
    echo "<td>$name</td>\n";
    echo "<td>$date</td>\n";
    echo "<td style=\"text-align: center;\">".
         "<input type=\"checkbox\" value=\"$name\" name=\"deadlines[]\"/>".
         "</td>\n";
    echo "</tr>\n\n";
}
?>
</table>
<div style="text-align: center;"><br/>
<button type="submit" name="delete" value="TRUE" onclick="return
confirm('Sei sicuro di voler eliminare le scadenze selezionate?')">
Elimina</button>
</div>
</form>

<?php
include("footer.php");
?>
