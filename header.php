<?php
/**
 * File con il contenuto dell'intestazione delle pagine
 */

// Controllo che impedisce il richiamo diretto del file include dall'esterno.
if( !defined('ASSOLI_SCRIPT') )
 {
     header('HTTP/1.1 404 Not Found');
     echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n<html>".
          "<head>\n<title>404 Not Found</title>\n</head>";
     echo "<body>\n<h1>Not Found</h1>\n<p>The requested URL ".
          $_SERVER['REQUEST_URI']." was not found on this server.</p>\n";
     echo "<hr>\n".$_SERVER['SERVER_SIGNATURE']."\n</body></html>\n";
     exit;
}

// Imposta il documento come XHTML per tutti i browser tranne Explorer,
// che non lo supporta e riceve quindi HTML.
if( preg_match( "/MSIE/", $_SERVER['HTTP_USER_AGENT'] ) )
    header("Content-Type: text/html; charset=utf-8");
else
    header("Content-Type: application/xhtml+xml; charset=utf-8");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="it" xml:lang="it">
<head>
<title>Squadra di traduzione italiana di KDE</title>

<style type="text/css">
    .cp-doNotDisplay { display: none; }
    @media aural, braille, handheld, tty {
        .cp-doNotDisplay { display: inline; speak: normal; }
    }
    .cp-edit { text-align: right; }
    @media print, embossed { .cp-edit { display: none; }}
</style>

<meta http-equiv="Content-Type"
        content="application/xhtml+xml; charset=utf-8"/>
<meta http-equiv="Content-Style-Type" content="text/css"/>
<meta name="description"
        content="Traduzioni in italiano del software della comunità KDE"/>
<meta name="MSSmartTagsPreventParsing" content="true"/>
<meta name="robots" content="all"/>
<meta name="no-email-collection"
        content="https://www.unspam.com/noemailcollection"/>

<link rel="icon" href="favicon.ico"/>
<link rel="shortcut icon" href="favicon.ico"/>
<link rel="stylesheet" media="screen" type="text/css" title="Foglio di stile
        di KDE" href="/capacity/includes/chihuahua/css.php"/>
<link rel="stylesheet" media="screen" type="text/css" title="Foglio di stile
        di KDE" href="/capacity/styles/plasmaMenu.css" />


<script type="text/javascript"
src="/capacity/javascripts/jquery-1.4.2.min.js">
</script>
<style type="text/css" media="all">
    @import "i18n_it.css";
</style>
</head>

<body id="cp-site-wwwkdeorg">

<ul class="cp-doNotDisplay">
<li><a href="#cp-content" accesskey="2">Skip to content</a></li>
<li><a href="#cp-menu" accesskey="5">Skip to link menu</a></li>
</ul>

<div class="root">
<div class="header">
<div class="toolbox">
<div id="location">
<img src="/capacity/images/kde.png" id="site_logo" alt="Logo" />
<div class="toolboxtext">
<a href="/">Pagina iniziale</a>
</div>
</div>
</div>
<div class="plasmamenu_box">
<div id="mainmenu_community" class="mainmenu_community">

<h2>Documenti</h2>
<div class="mainmenu_community_sub plasmamenu_box_submenu">
<div class="menubox_top"></div>
<div class="menubox_body">
<div class="mainmenu_community_animation"></div>
<div class="mainmenu_community_content">
<ul>
<li class="index">
<a href="nuovonuovo.php">Per i nuovi traduttori</a></li>
<li class="index">
<a href="https://community.kde.org/KDE_Localization/it">Wiki e
glossario</a></li>
<li class="index">
<a href="traduttori.php">I traduttori</a></li>
</ul>
</div>
</div>
<div class="menubox_bottom"></div>
</div>
</div>


<div id="mainmenu_workspaces" class="mainmenu_workspaces">
<h2>Dati</h2>
<div class="mainmenu_workspaces_sub plasmamenu_box_submenu">
<div class="menubox_top"></div>
<div class="menubox_body">
<div class="mainmenu_workspaces_animation"></div>
<div class="mainmenu_workspaces_content">
<ul>
<li class="index"><a href="ricerca.php">Ricerca</a></li>
<li class="index"><a href="https://community.kde.org/Schedules">
Scadenze</a></li>
<li class="index"><a href="stat.php">Statistiche</a></li>
<li class="index"><a href="https://bugs.kde.org/buglist.cgi?&amp;product=i18n&amp;component=it&amp;bug_status=UNCONFIRMED&amp;bug_status=CONFIRMED&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">Bug aperti</a></li>
</ul>
</div>
</div>
<div class="menubox_bottom"></div>
</div></div>

<div id="mainmenu_applications" class="mainmenu_applications">
<h2>Gestione dei file</h2>
<div class="mainmenu_applications_sub plasmamenu_box_submenu">
<div class="menubox_top"></div>
<div class="menubox_body">
<div class="mainmenu_applications_animation"></div>
<div class="mainmenu_applications_content">
<ul>
<li class="index"><a href="pacchetti.php">Pacchetti e file</a></li>
<li class="index">
<a href="decidi.php">Le tue richieste</a>
</li>
<li class="index"><a href="register.php">Registrati</a></li>
<li class="index">
<a href="register.php?modify=TRUE">Modifica i tuoi dati</a>
</li>
<li class="index">
<a href="resetpassword.php">Azzera la password</a>
</li>
</ul>
</div>
</div>
<div class="menubox_bottom"></div>
</div></div>

<div id="mainmenu_developer_platform" class="mainmenu_developer_platform">
<h2>Liste</h2>
<div class="mainmenu_developer_platform_sub plasmamenu_box_submenu">
<div class="menubox_top"></div>
<div class="menubox_body">
<div class="mainmenu_developer_platform_animation"></div>
<div class="mainmenu_developer_platform_content">
<ul>
<li class="index">
<a href="https://mail.kde.org/mailman/listinfo/kde-i18n-it">Italiana</a>
</li>
<li class="index">
<a href="https://lists.kde.org/?l=kde-i18n-it&amp;r=1&amp;w=2">(Archivio)</a>
</li>
<li class="index">
<a href="https://mail.kde.org/mailman/listinfo/kde-i18n-doc">Internazionale</a>
</li>
<li class="index">
<a href="https://lists.kde.org/?l=kde-i18n-doc&amp;r=1&amp;w=2">(Archivio)</a>
</li>
</ul>
</div>
</div>
<div class="menubox_bottom"></div>
</div></div>


<?php
require_once( 'configurazione.php' );
// Controlla l'autenticazione dell'utente per il menu con funzioni avanzate

if ( isset($_SERVER['PHP_AUTH_USER']) ) {

    require_once( 'connessione.php' );

    $query = "SELECT first, idTranslator, admin FROM translators ".
             "WHERE email= ?";
    $stmt = $db->prepare($query);
    $stmt->bind_param('s', $_SERVER['PHP_AUTH_USER']);
    $stmt->execute();
    $stmt->bind_result($name, $id, $isAdmin);
    if( $stmt->fetch() ) {
?>

<div id="mainmenu_support" class="mainmenu_support">
<h2>Ciao, <?php echo $name ?>!</h2>
<div class="mainmenu_support_sub plasmamenu_box_submenu">
<div class="menubox_top"></div>
<div class="menubox_body">
<div class="mainmenu_support_animation"></div>
<div class="mainmenu_support_content">
<ul>
<li class="index">
<a href="file.php?translator=<?php echo $id; ?>&amp;status=unfinished">Lavoro
da fare</a>
</li>
<?php
        if ( $isAdmin ) {
            echo '<li class="index">'.
                '<a href="admin-requests.php">Richieste in corso</a></li>';
            echo '<li class="index">'.
                '<a href="admin-users.php">Utenti</a></li>';
            echo '<li class="index">'.
                '<a href="admin-deadlines.php">Scadenze</a></li>';
            echo '<li class="index">'.
                '<a href="admin-files.php">Assegnazione dei file</a></li>';
        }
?>
</ul>
</div>
</div>
<div class="menubox_bottom"></div>
</div></div>

<?php
    }
    $stmt->close();
}

?>

</div>
</div>


<div class="content">
<script type="text/javascript"
        src="/capacity/javascripts/jquery-1.4.2.min.js"></script>
<script type="text/javascript"
        src="/capacity/javascripts/plasmaMenu.min.js"></script>
<script type="text/javascript"
        src="/capacity/javascripts/jquery.cycle.all.min.js">
</script>
<div id="main">

<!-- Qui finisce l'header -->

