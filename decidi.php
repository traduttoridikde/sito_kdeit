<?php
/**
 * File per decidere se approvare o rifiutare l'assegnazione di un file.
 */

define( "ASSOLI_SCRIPT", 1 );

require_once( "configurazione.php" );
require_once( "connessione.php" );

include( "identifica.php" );
include( "header.php" );
include( "functions.php" );

$decision = chkvar( $_POST['decision'] );
$PHP_SELF = $_SERVER['PHP_SELF'];

$query = "SELECT idTranslator FROM translators WHERE email= ?";
$stmt  = $db->prepare($query);
$stmt->bind_param('s', $_SERVER['PHP_AUTH_USER']);
$stmt->execute();
$stmt->bind_result($id);
$stmt->fetch();
$stmt->close();

if ( $decision ) {

    $actions = chkvar( $_POST['actions'] );
    $comment = chkvar( $_POST['comment'] );

    // Riorganizza dati in array $applicants per richiedente, azione ed esito.
    $applicants  = array();
    foreach ( $actions as $action ) {
        if ( is_numeric($action) && $action == 0 ) // Nessuna decisione presa
            continue;
        if ( is_numeric($action) ) { // Accetta richiesta con questo numero
            // Autenticazione: verifica che idTranslator corrisponda a $id
            $query = 'SELECT file, package, type, review, idFrom '.
                     'FROM requests NATURAL JOIN translators '.
                     'WHERE idRequest= ? AND email= ? AND decision IS NULL';
            $stmt  = $db->prepare($query);
            $stmt->bind_param('ss', $action, $_SERVER['PHP_AUTH_USER']);
            $stmt->execute();
            $stmt->bind_result($file, $package, $type, $review, $idFrom);

            // Se nessun risultato, o non è il traduttore giusto,
            // o la decisione è già stata presa (pagina ricaricata per sbaglio)
            // fermati e passa al prossimo $action
            if ( !$stmt->fetch() )
                continue;

            handleRequest( $action, TRUE, $db );

            if( !array_key_exists( $idFrom, $applicants ) )
                $applicants[$idFrom] = array();
            $note  = "$type/$package/$file";
            $note .= $review ? ' (revisione)' : ' (traduzione)';
            $note .= ': Accettato';
            $applicants[$idFrom][] = $note;

            $stmt->close();
            // Metti in $action l'identificativo del file e passa a
            // rimuovere eventuali altre richieste
            $action = "$review/$type/$package/$file";
        }
        // Rifiuta tutte le richieste per il file indicato in $action
        list( $review, $type, $package, $file ) = explode( '/', $action );

        // Verifica che la richiesta arrivi dal titolare del file,
        // Altrimenti passa al prossimo
        if ( $id != findOwner( $type, $package, $file, $review, $db ) )
            continue;

        // Trova tutte richieste pendenti per il file indicato
        $query = 'SELECT idRequest, idFrom FROM requests '.
                 'WHERE file= ? AND package= ? AND type= ? AND review= ? '.
                       'AND decision IS NULL';
        $stmt  = $db->prepare($query);
        $stmt->bind_param('sssi', $file, $package, $type, $review);
        $stmt->execute();
        $stmt->bind_result($idRequest, $idFrom);

        // Rifiutale tutte
        while ( $stmt->fetch() ) {
            handleRequest( $idRequest, FALSE, $db );

            if( !array_key_exists( $idFrom, $applicants ) )
                $applicants[$idFrom] = array();
            $note  = "$type/$package/$file";
            $note .= $review ? ' (revisione)' : ' (traduzione)';
            $note .= ': Rifiutato';
            $applicants[$idFrom][] = $note;
        }
        $stmt->close();
    }

    // Trova mail e nome di ogni richiedente e del decisore
    $addresses = array();
    $fullnames = array();
    $query = "SELECT idTranslator, email, CONCAT_WS(' ', first, last) AS name ".
             "FROM translators WHERE idTranslator=$id";
    $query = $db->escape_string($query);
    foreach ( $applicants as $idFrom => $note )
        $query .= " OR idTranslator=$idFrom";
    $result = $db->query($query);
    while( $r = $result->fetch_assoc() ) {
        $addresses[$r['idTranslator']] = $r['email'];
        $fullnames[$r['idTranslator']] = $r['name'];
    }
    $result->free();

    // Per ogni chiave di $applicants, manda una mail di avviso.
    $reply_to = $fullnames[$id].' <'.$addresses[$id].'>';
    $subject  = "Decisione sulla richiesta di acquisizione di un file";
    foreach( $applicants as $key => $notes ) {
        $to_address = $fullnames[$key].' <'.$addresses[$key].'>';
        $message = $fullnames[$id]." ha deciso in merito alle tue richieste:\n";
        foreach( $notes as $note )
            $message .= $note."\n";

        if ( $comment )
            $message .= "\nMessaggio da parte di ".$fullnames[$id].":\n\n";
            $message .= $comment;
        $message = wordwrap( stripslashes( $message ) );
        inviaPosta( $to_address, $subject, $message, $reply_to );
    }
}

$query = 'SELECT idRequest, file, package, type, review, '.
                "CONCAT_WS( ' ', first, last ) AS name ".
         'FROM requests JOIN translators ON idFrom=translators.idTranslator '.
         "WHERE requests.idTranslator= ? AND decision IS NULL ".
         'ORDER BY name ASC';
$stmt  = $db->prepare($query);
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->bind_result($idRequest, $file, $package, $type, $review, $name);

$AllOptions = array();
while ( $stmt->fetch() ) {
    $fullFile = "$review/$type/$package/$file";
    if( !array_key_exists( $fullFile, $AllOptions ) )
        $AllOptions[$fullFile] = array();
    $AllOptions[$fullFile][] = "<option value=\"$idRequest\" >Assegna a ".
                               "$name</option>\n";
}
$stmt->close();

if ( empty( $AllOptions ) ) {
?>
<fieldset class="it_feedback">
<legend>Nessuna richiesta</legend>
Nella banca dati non risultano richieste per te. Va tutto bene.
</fieldset>
<?php
} else {
?>
<form action="<?php echo $PHP_SELF; ?>" method="post">
<table class="it_pack" style="margin: auto;">
<tr>
<th>File richiesto</th>
<th>Decisione</th>
</tr>
<?php
    foreach( $AllOptions as $file => $options ) {
        echo "<tr>\n" ;
        echo "<td>";
        echo substr($file,2);
        echo $file[0] ? " (Revisione)" : " (Traduzione)";
        echo "</td>\n" ;
        echo "<td>\n" ;
        echo "<select name=\"actions[]\">\n" ;
        echo "<option selected=\"selected\" value=\"0\">Procrastina</option>\n";
        echo "<option value=\"$file\">Tieni</option>\n";
        foreach( $options as $option )
            echo $option;
        echo "</select>\n";
        echo "</td>\n";
        echo "</tr>\n\n";
    }
?>
</table>

<p>In caso di richieste multiple per lo stesso file, una decisione rifiuterà
tutte le altre.<br/>
L'eventuale messaggio qui sotto sarà inviato <strong>a tutti</strong>
quelli a cui hai accettato o rifiutato l'assegnazione dei file.</p>

<textarea name="comment" cols="50" rows="6"></textarea><br />
<button type="submit" value="TRUE" name="decision">Conferma</button>
<button type="reset">Azzera</button>
</form>

<?php
}

include("footer.php");

?>
